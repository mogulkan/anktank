# ankTank is PHP web application filter(WAF).

### Declaimer.
This software given as is, with any obligation or responsibility for using it.
I am very disappointed by reduced functionality. But I need gather some feedback from you for next ankTank developing and evolving stage.
If I am developing ankTank since 2013. The quality was fine for me, but I am still haven't completely satisfied.

### Benefits.
* Encapsulation, checking variable code replaced by one function call.
* All rules in one place, less chance to make mistake, easy to edit.
* Dedicated attention. It's easy to set task or dedicate a programmer to checking and setting variable rule, instead of each programmer go all own style how much check user input.


### Features.
1. Checking known and allowed variables.
2. Checking allowed type of a variable.
3. Checking incoming source of variables($_GET / $_POST / etc.). 
4. Recursive checking values in array with all rules described below.
5. Recursive checking values by custom callback function.
6. Checking minimal / maximal / equal /  emptiness value.
7. Checking value by known common values.
8. Checking value by known forbidden values.
9. Checking value by regular expression against each regExp or anyone.
10. Checking value by regular expression for forbidden values(all or none shall pass).
11. Automated value chopping if necessary.
12. Data type conversion.
13. Easy setting default values for omitting or invalid values.
14. Easy to use only one function to check and get variable.   
15. Easy disable incoming variables(debug / dev / outdated).

#### Documentation.
Check example folder for further information and examples. 

#### Want to help?
1. Write article / video / slides.
2. Write / create video tutorial
3. Create documentation.
4. Create plugin for CMS (WordPress, Joomla, Drupal, etc...)
5. Create a set of rules for common data, regExp and etc...
6. Encourage / discourage me - rank repository project. 
7. Send feedback / bugs / ideas.
8. Write a better WAF.
9. Donate.    
    
#### If you found bug...
Have you found a bug / security breach in ankTank?
 
Use ./ankTank/example/example_0_breach.php template to submit bug / breach reproduction script.

#### Future Features & plans.
    JS      plugin for browsers
    HTML    generator for input tags    
    PHP     Cache subsytem
    PHP     Cache adapter     to SQL / noSQL vaults.     
    PHP     Cache generator from SQL / noSQL vaults.    
    PHP     Rule extending.
    PHP     Rule for some standart variables.
    PHP     Injection detection.
    PHP     Autoconfig - create rules by analyze request.
    PHP     Dynamic naming for variables.
    example Example intergration with application router.
    support Write automatic tests and other source code checks, make friendly to CI, and other automatization.
    Plugin  support for file checking.

Dynamic naming.

With feature will changing url names to something else:

from = alice &  to = Bob & money = 10 & currency = 1.

tYRdr = alice & Bglm = Bob & lCqUb = 10 & ZuRdT = 1.

Frequency and other stuff will customized.
Token may carry hint or namespace will being noticed by other marks.

#### Installation
Include src/require.php to your project.
Check example for further details and check how it works.
 
