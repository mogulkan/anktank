# ankTank documentation and example.

### How to use ankTank.
1. Create your own security class - myAnkTank and extend it from \mogulkan\ankTankA.
2. Create constant for your variables in your class (myAnkTank).  
2. Create rules by setting by redeclare A_RULE_CACHE constant in your class (myAnkTank).
4. Use $result = myAnkTank::validateAndPut(myAnkTank::VAR_NAME, $ANY_VAR_NAME);
5. Check return value and use $ANY_VAR_NAME. That is all.


### Examples of usage.
To check example run one of scripts below
##### Example #0
 If you found a bug or breach in ankTank use this template to create reproduction script. Further instruction placed into template file. 
 - vendor/mogulkan/ankTank/example/ankTankExample_0/example_0_breach.php
 
##### Example #1 
- ankTank/example/ankTankExample_1_value.php  - showed how filter values.
- ankTank/example/ankTankExample_1_array.php  - showed how filter array of values.
- ankTank/example/ankTankExample_1_breach.php - if you found bug or security breach in example use it to submit bug report.



### How to set rule array.
Rule array have 15 parameters, but only 2 must to be set, all other optional.
If you need more information check PHPDOC in \mogulkan\ankTankI and \mogulkan\ankTankA. 

Must be present in redeclared A_RULE_CACHE:

| # | PHP type    | Rule array key       | Description                                                                    |
| --- |:---------:| --------------------:|--------------------------------------------------------------------------------|
| 1 | **INT**     | C_RULE_BIN_CMN       | Binary set by ankTankA::F_CMN_*. Flags description placed below.               |
| 2 | **INT**     | C_RULE_BIN_FROM      | Binary set by ankTankA::F_FROM__*. Flags description placed below.             |


Optional settings.

| # | PHP type    | Rule array key       | Description                                                                    |
| --- |:---------:| --------------------:|--------------------------------------------------------------------------------|
| 3 | **INT**     | C_RULE_MORE          | Variable length should be more this value.                                     |
| 4 | **INT**     | C_RULE_LESS          | Variable length should be less this value.                                     |
| 5 | **INT**     | C_RULE_MIN           | Variable length should be more this value or equal.                            |
| 6 | **INT**     | C_RULE_MAX           | Variable length should be more this value or equal.                            |
| 7 | **INT**     | C_RULE_EQUAL         | Variable length should be equal to this value.                                 |
| 8 | **CALLABLE**| C_RULE_CALLBACK      | Callback to invoke custom function to check value.                             |
| 9 | **INT**     | C_RULE_RECURSION     | Number of sub arrays for variable(Only arrays).                                |
| 10| **ARRAY**   | C_RULE_CMN           | Array with common values for variable.                                         |
| 11| **ARRAY**   | C_RULE_CMN_REGEX_ALL | Array with regExp. Value should match all of them.                             |
| 12| **ARRAY**   | C_RULE_CMN_REGEX_ANY | Array with regExp. Value should match one or none of them.                     |
| 13| **ARRAY**   | C_RULE_FRB           | Array with forbidden values for variable. Should not match to anyone.          |
| 14| **ARRAY**   | C_RULE_FRB_REGEX_ALL | Array with regExp. Value should match all of them. Should not match to all.    |
| 15| **ARRAY**   | C_RULE_FRB_REGEX_ANY | Array with regExp. Value should match all of them. Should not match to anyone. |


#### Binary flags 
For C_RULE_BIN_CMN: 

| # | Notice      | Binary constant name      | Description                                                                       |
| --- |:---------:| -------------------------:|-----------------------------------------------------------------------------------|
| 1 |             | F_CMN_DISABLED            | Disable variable. Rule is set, but check is failed.                               |
| 2 |             | F_CMN_EMPTY_VAL           | Allow to use empty value for variable.(\empty())                                  |
| 3 |             | F_CMN_LOG                 | Successfully check is logged.                                                     |
| 4 |             | F_CMN_LOG_ERROR           | Check fail status is logged.                                                      |
| 5 |             | F_CMN_ONLY_VALUE          | Only common values in C_RULE_CMN is allowed for variable.                         |
| 6 | regExp      | F_CMN_AT_LEAST_ONE_REGEXP | Value must be matched at least to one regExp in C_RULE_CMN_REGEX_ANY              |
| 7 |             | F_CMN_TRY_FIX_LEN         | Value is padded or chopped to more / less / min / max limit.                      |
| 8 | only arrays | F_CMN_ARRAY_SIMPLE        | Rule for sub elements searches by variable name.                                  |
| 9 | only arrays | F_CMN_ARRAY_COMPLEX       | Rule for sub elements searches by variable name and suffix.                       |
| 10| only arrays | F_CMN_ARRAY_WHOLE         | All sub elements should pass check for a parent array check pass.                 |
| 10| only arrays | F_CMN_RECURSION_NULL      | Sub elements set to null if recursion limit was reached.                          |
| 10| only arrays | F_CMN_RECURSION_EMPTY     | Sub elements set to empty if recursion limit was reached.                         |
| 10| only arrays | F_CMN_RECURSION_FAIL      | Array fail the check.                                                             |
| 10| only arrays | F_CMN_RECURSION_PASS      | Sub elements check is skipped, parent array check is passed. ***!!!DANGEROUS!!!***|


##### A_RULE_CACHE constant structure.
ankTankA::A_RULE_CACHE is multidemensional constant array.

Pattern: ankTankA::A_RULE_CACHE[ TYPE ][ *variable_name_length* ][ *variable_name* ] = array(ankTankI::C_RULE_* => ***, );

Example: ankTankA::A_RULE_CACHE[ [ ankTankI::C_INT ]][ 7 ][ ankTankI::VAR_NAME ] = array(ankTankI::C_RULE_* => ***, );

Check ankTank/example/ankTankExample_1/ankTankExample_1 to see how variable and rule declarated.
