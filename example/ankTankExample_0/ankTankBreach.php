<?php
declare(strict_types = 1);

namespace mogulkan\ankTank;


#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-     SET VARIABLE AND RULE    -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
class ankTankBreach extends ankTankA
{
  /* Step #1 Set variable for bug / breach demonstration. */
  public const T_VAR_NAME  = 'varName';//
  
  /* Step #2 Set rule for the variable. Use dummy rule below as template.*/
  public const A_RULE_CACHE = array(
  ankTankI::C_ARRAY => array(
    7 => array(
      ankTankBreach::T_VAR_NAME => array(
//        ankTankI::C_RULE_BIN_CMN   => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_ARRAY_COMPLEX | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
//        ankTankI::C_RULE_BIN_FROM  => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
//        ankTankI::C_RULE_MORE      => 0, ankTankI::C_RULE_MIN => 1, ankTankI::C_RULE_EQUAL => '',
//        ankTankI::C_RULE_LESS      => 4, ankTankI::C_RULE_MAX => 2,
//        ankTankI::C_RULE_RECURSION => ankTankA::C_RECURSION_PROTECTOR,
//        ankTankI::C_RULE_CALLBACK  => '',
//        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ALL => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(),
//        ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ALL => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array(),
      ),
    ),
  ),
  
  ankTankI::C_BOOL   => array(),
  ankTankI::C_INT    => array(),
  ankTankI::C_FLOAT  => array(),
  ankTankI::C_STRING => array(),
);//  
  
  
  
  
  
  
  
  
  
  
}





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-     GLOBAL PHP VARIABLES     -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
/* Step #3 Set variable and it's value demonstration. */
//$_POST[    ankTankBreach::T_VAR_NAME ];
//$_GET[     ankTankBreach::T_VAR_NAME ];
//$_COOKIE[  ankTankBreach::T_VAR_NAME ];
//$_SERVER[  ankTankBreach::T_VAR_NAME ];





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-     CALL VULNERABLE METHOD   -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
/* Step #4 Call vulnerable method. */
$result = ankTankBreach::validateAndPut(ankTankBreach::T_VAR_NAME, $var);





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-    INFORMATION ABOUT ISSUE   -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
/* Step #5 Provide some explanation.
  1. Why do you think your variable should pass or not pass the check.
  2. What behaviour is desirable?
  3. Any extra information which you think may be helpful. 
*/
if($result)
{
# Additional information why variable should pass the check.
}
else
{
# Or additional information why variable shouldn't pass the check.
}






/*////////////////////////////////////////////////////////////////////////////////////////////////*/
/* License:     GPL3???                                                                           */
/* Created:     2018-05-08                                                                        */
/* Edited:      2018-05-08                                                                        */
/* Authors: mogulkan                                                                              */
/* Author crypto signature:                                                                       */
/* Title:                                                                                         */
/* Type:                                                                                          */
/* Purpose                                                                                        */
/* Description:                                                                                   */
/* Notice:                                                                                        */
/*////////////////////////////////////////////////////////////////////////////////////////////////*/
