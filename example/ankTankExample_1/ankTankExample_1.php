<?php
declare(strict_types = 1);

namespace mogulkan\ankTank;

/* Declaimer #1
 * Mixing declaration code, execution code, HTML markup, CSS style is corrupt behavior for good programer.
 * In this script it appropriated to expose simply "one file" example.
 */
/* Declaimer #2
 * Using global PHP arrays with ankTank is bad habit.
 * Try avoid to access the global PHP arrays, use ankTank instead. 
 * In this script access to $_POST and $_GET have only demonstration purpose.
 */
/* Declaimer #2
 * Using input without restrictions is poor practice.
 * In this script input restriction don't used to provide more and easy way to demonstrate ability for variable checking.
 */
class ankTankExample_1 extends ankTankA
{
  public const T_NAME_F           =    'nameF';//
  public const T_NAME_M           =    'nameM';//
  public const T_NAME_L           =    'nameL';//
  public const T_BANK_CARD_NUMBER = 'bankCard';//
  public const T_BANK_CARD_CVV    =      'cvv';//
  public const T_BANK_CARD_EXP_Y  =     'expY';//
  public const T_BANK_CARD_EXP_M  =     'expM';//
  public const T_AGREED           =   'agreed';//
  public const T_RADIO_SET        = 'radioSet';//
  public const T_CURRENCY         = 'currency';//
  public const T_QUANTITY         = 'quantity';//
  
  public const D_VAL_BANK_CARD_FRB_ALL = array(
    '^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$',
    '^(4903|4905|4911|4936|6333|6759)[0-9]{12}|(4903|4905|4911|4936|6333|6759)[0-9]{14}|(4903|4905|4911|4936|6333|6759)[0-9]{15}|564182[0-9]{10}|564182[0-9]{12}|564182[0-9]{13}|633110[0-9]{10}|633110[0-9]{12}|633110[0-9]{13}$',
  );//
  
  public const D_VAL_BANK_CARD_FRB_ANY = array(
    '^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$',
  );//
  
  public const A_VAL_BANK_CARD_CMN_ANY = array(
    '^3[47][0-9]{13}$',
    '^(6541|6556)[0-9]{12}$',
    '^389[0-9]{11}$',
    '^3(?:0[0-5]|[68][0-9])[0-9]{11}$',
    '^63[7-9][0-9]{13}$',
    '^(?:2131|1800|35\d{3})\d{11}$',
    '^9[0-9]{15}$',
    '^(6304|6706|6709|6771)[0-9]{12,15}$',
    '^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$',
    '^(6334|6767)[0-9]{12}|(6334|6767)[0-9]{14}|(6334|6767)[0-9]{15}$',
    '^(4903|4905|4911|4936|6333|6759)[0-9]{12}|(4903|4905|4911|4936|6333|6759)[0-9]{14}|(4903|4905|4911|4936|6333|6759)[0-9]{15}|564182[0-9]{10}|564182[0-9]{12}|564182[0-9]{13}|633110[0-9]{10}|633110[0-9]{12}|633110[0-9]{13}$',
    '^(62[0-9]{14,17})$ ',
    '^4[0-9]{12}(?:[0-9]{3})?$',
    '^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$',
  );//
  
  public const A_VAL_CURRENCY_CMN = array(
    'CHF',
    'EUR',
    'USD',
  );//
  public const A_VAL_CURRENCY_FRB = array(
    'ZWD',
  );//
  
  public const A_MONTH = array(
    'january',
    'february',
    'march',
    'april',
    'may',
    'june',
    'july',
    'august',
    'september',
    'november',
    'december',
  );//
  
  /* As you can see the array is huge even in simply example script. 
   * Insistently I suggest to you 
   * 1. Use sharding by type and / or variable length. 
   * 2. Use a static cache in your favorite framework. 
   */
  public const A_RULE_CACHE = array(
  ankTankI::C_ARRAY => array(
    3 => array(
      ankTankExample_1::T_BANK_CARD_CVV => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),

    4 => array(
      ankTankExample_1::T_BANK_CARD_EXP_Y => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_BANK_CARD_EXP_M => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST ,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),

    5 => array(
      ankTankExample_1::T_NAME_F => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_NAME_M  => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_NAME_L => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),

    6 => array(
      ankTankExample_1::T_AGREED  => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),

    8 => array(
      ankTankExample_1::T_RADIO_SET => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
//        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_QUANTITY => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_COMPLEX | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_BANK_CARD_NUMBER => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
      ),
      ankTankExample_1::T_CURRENCY => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE => 0,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ARRAY_SIMPLE | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
//        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
  ),



  

  ankTankI::C_BOOL => array(
    6 => array(
      ankTankExample_1::T_AGREED => array(
        ankTankI::C_RULE_MIN      => 0, ankTankI::C_RULE_MORE =>  -1,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 3, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_EMPTY_VAL,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
    8 => array(
      ankTankExample_1::T_RADIO_SET => array(
        ankTankI::C_RULE_MIN      => 0, ankTankI::C_RULE_MORE =>  -1,
        ankTankI::C_RULE_MAX      => 2, ankTankI::C_RULE_LESS => 3, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_EMPTY_VAL,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
//        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
  ),





  ankTankI::C_INT => array(
    3 => array(
      ankTankExample_1::T_BANK_CARD_CVV => array(
        ankTankI::C_RULE_MIN      =>  100, ankTankI::C_RULE_MORE =>  99,
        ankTankI::C_RULE_MAX      => 1000, ankTankI::C_RULE_LESS => 999, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_EMPTY_VAL | ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
    4 => array(
      ankTankExample_1::T_BANK_CARD_EXP_M => array(
        ankTankI::C_RULE_MIN      => 1, ankTankI::C_RULE_MORE =>  0,
        ankTankI::C_RULE_MAX      => 12, ankTankI::C_RULE_LESS => 13, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_BANK_CARD_EXP_Y => array(
        ankTankI::C_RULE_MIN      => 19, ankTankI::C_RULE_MORE => 18,
        ankTankI::C_RULE_MAX      => 24, ankTankI::C_RULE_LESS => 25, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
    8 => array(
      ankTankExample_1::T_QUANTITY => array(
        ankTankI::C_RULE_MIN      =>  1, ankTankI::C_RULE_MORE =>   0,
        ankTankI::C_RULE_MAX      => 99, ankTankI::C_RULE_LESS => 100, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG  | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
//        ankTankI::C_RULE_CMN      => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
    9 => array(
      ankTankExample_1::T_QUANTITY . ankTankA::T_ARRAY_DELIMITER => array(
        ankTankI::C_RULE_MIN      => 100 , ankTankI::C_RULE_MORE =>  99, //ankTankI::C_RULE_EQUAL    => 3,
        ankTankI::C_RULE_MAX      => 1000, ankTankI::C_RULE_LESS => 999,
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG  | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        //        ankTankI::C_RULE_CMN => ankTankExample_1::A_VAL_CURRENCY_CMN, ankTankI::C_RULE_CMN_REGEX_ANY => array(),
        //        ankTankI::C_RULE_FRB => ankTankExample_1::A_VAL_CURRENCY_FRB, ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
  ),

  ankTankI::C_STRING => array(
    4 => array(
      ankTankExample_1::T_BANK_CARD_EXP_M => array(
        ankTankI::C_RULE_MIN      => 3, ankTankI::C_RULE_MORE =>  2,
        ankTankI::C_RULE_MAX      => 9, ankTankI::C_RULE_LESS => 13, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_ONLY_VALUE,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN      => ankTankExample_1::A_MONTH, //ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
    5 => array(
      ankTankExample_1::T_NAME_F => array(
        ankTankI::C_RULE_MIN      =>  2, ankTankI::C_RULE_MORE =>  2,
        ankTankI::C_RULE_MAX      => 16, ankTankI::C_RULE_LESS => 17, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_NAME_M => array(
        ankTankI::C_RULE_MIN      =>  2, ankTankI::C_RULE_MORE =>  2,
        ankTankI::C_RULE_MAX      => 16, ankTankI::C_RULE_LESS => 17, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
      ankTankExample_1::T_NAME_L => array(
        ankTankI::C_RULE_MIN      =>  2, ankTankI::C_RULE_MORE =>  2,
        ankTankI::C_RULE_MAX      => 16, ankTankI::C_RULE_LESS => 17, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
    8 => array(
      ankTankExample_1::T_BANK_CARD_NUMBER => array(
        ankTankI::C_RULE_MIN      => 16, ankTankI::C_RULE_MORE => 15,
        ankTankI::C_RULE_MAX      => 64, ankTankI::C_RULE_LESS => 65, ankTankI::C_RULE_CALLBACK => '\mogulkan\pileNumber', ankTankI::C_RULE_RECURSION => 2,
        ankTankI::C_RULE_BIN_CMN                                                                                                                      => ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM                                                                                                                     => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN                                                                                                                          => array(), ankTankI::C_RULE_CMN_REGEX_ALL =>                                   array(), ankTankI::C_RULE_CMN_REGEX_ANY => ankTankExample_1::A_VAL_BANK_CARD_CMN_ANY,
        ankTankI::C_RULE_FRB                                                                                                                          => array(), ankTankI::C_RULE_FRB_REGEX_ALL => ankTankExample_1::D_VAL_BANK_CARD_FRB_ALL, ankTankI::C_RULE_FRB_REGEX_ANY => ankTankExample_1::D_VAL_BANK_CARD_FRB_ANY
      ),
      ankTankExample_1::T_CURRENCY => array(
        ankTankI::C_RULE_MIN      => 3, ankTankI::C_RULE_MORE => 2, ankTankI::C_RULE_EQUAL    => 3,
        ankTankI::C_RULE_MAX      => 3, ankTankI::C_RULE_LESS => 4, ankTankI::C_RULE_CALLBACK => '',
        ankTankI::C_RULE_BIN_CMN  => ankTankI::F_CMN_LOG | ankTankI::F_CMN_LOG_ERROR,
        ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_GET | ankTankI::F_FROM_POST,
        ankTankI::C_RULE_CMN => ankTankExample_1::A_VAL_CURRENCY_CMN, ankTankI::C_RULE_CMN_REGEX_ANY => array(),
        ankTankI::C_RULE_FRB => ankTankExample_1::A_VAL_CURRENCY_FRB, ankTankI::C_RULE_FRB_REGEX_ANY => array()
      ),
    ),
  ),
);//
  
}





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\- CALLBACK FUNCTION DECLARATION-/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
function pileNumber(int $from, $var, &$value, bool $pbChop, ?int $recursionProtector, int $varLen, int $valType): int
{
# Assign variables.
  $TMP_number = array_flip(array(0,1,2,3,4,5,6,7,8,9));
  
# Processing.
  $TMP_value  = '';
  foreach(str_split($value) as $val)
  {
    if(isset($TMP_number[ $val ]))     {$TMP_value .= $val;}
  }
  
  $result = ankTankExample_1::checkVar($from, $var, $TMP_value, $pbChop, ankTankI::D_RECURSION_DISABLED);
  
# Return.
  if(ankTankI::R_VAR_CHK_PASS == $result)     {$value = $TMP_value;}
  
  return $result;
}





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-  ERROR FUNCTION DECLARATION  -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
$rtnHtmlError = function(string $varName, ?int $valType = null): string 
{
  if(is_null($valType)){$intStatus = ankTankExample_1::getVarStatus(                                                          $varName);}
  else                 {$intStatus = ankTankExample_1::getVarStatusByType($valType, ankTankExample_1::rtnVarLength($varName), $varName);}
  
  if(is_null($intStatus))
  {
    if(isset($_REQUEST[ $varName ]))
    {
      // For unknown variables we don't save error code, it's can be obtained by direct method call. 
          if(isset($_POST[ $varName ])){$intStatus = ankTankExample_1::validate($varName, $_POST[ $varName ], ankTankI::F_FROM_POST);}
      elseif(isset($_GET[  $varName ])){$intStatus = ankTankExample_1::validate($varName, $_GET[  $varName ], ankTankI::F_FROM_GET );}
      else                             {$intStatus =                                                          ankTankI::E_VAR_UNK   ;}
    }
    else
    {
      $intStatus = ankTankI::E_VAR_NOT_SEND;
    }
  }
  elseif(empty(ankTankI::A_LANG_ERR_MSG[ $intStatus ])){$intStatus = ankTankI::E_VAR_UNK_RULE;}
  elseif(      ankTankI::R_VAR_CHK_PASS          == $intStatus
            || ankTankI::R_VAR_CHK_PASS_FAIL     == $intStatus
            || ankTankI::R_VAR_CHK_PASS_NOT_SEND == $intStatus
  ){
    $dataCheck = ankTankI::R_VAR_CHK_PASS        == $intStatus ? 1 : 2;
  
    return "<p data-check='$dataCheck'>Check passed.<br>Variable: '$varName'.<br> Notice: '".ankTankI::A_LANG_ERR_MSG[ $intStatus ] ."'</p>";
  }
  elseif(!isset($_REQUEST[ $varName ])){$intStatus = ankTankI::E_VAR_NOT_SEND;}
  
  return "<p data-check='0'>Check failed.<br>Variable: '$varName'.<br>Error description: '".ankTankI::A_LANG_ERR_MSG[ $intStatus ].'"</p>';
};







/*////////////////////////////////////////////////////////////////////////////////////////////////*/
/* License:     GPL3???                                                                           */
/* Created:     2018-05-08                                                                        */
/* Edited:      2018-05-08                                                                        */
/* Authors: mogulkan                                                                              */
/* Author crypto signature:                                                                       */
/* Title:                                                                                         */
/* Type:                                                                                          */
/* Purpose                                                                                        */
/* Description:                                                                                   */
/* Notice:                                                                                        */
/*////////////////////////////////////////////////////////////////////////////////////////////////*/
