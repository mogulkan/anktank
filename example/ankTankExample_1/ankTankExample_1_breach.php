<?php
declare(strict_types = 1);

namespace mogulkan\ankTank;

require 'ankTankExample_1.php';

/* Use this template if you found bug / breach in ankTankExample_1 rules.*/
#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-     GLOBAL PHP VARIABLES     -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
/* Step #3 Set variable and it's value demonstration. */
//$_POST[    ankTankBreach::T_VAR_NAME ];
//$_GET[     ankTankBreach::T_VAR_NAME ];
//$_COOKIE[  ankTankBreach::T_VAR_NAME ];
//$_SERVER[  ankTankBreach::T_VAR_NAME ];





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-     CALL VULNERABLE METHOD   -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
/* Step #4 Call vulnerable method. */
$result = ankTankBreach::validateAndPut(ankTankBreach::T_VAR_NAME, $var);





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-    INFORMATION ABOUT ISSUE   -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
/* Step #5 Provide some explanation.
  1. Why do you think your variable should pass or not pass the check.
  2. What behaviour is desirable?
  3. Any extra information which you think may be helpful. 
*/
if($result)
{
# Additional information why variable should pass the check.
}
else
{
# Or additional information why variable shouldn't pass the check.
}






/*////////////////////////////////////////////////////////////////////////////////////////////////*/
/* License:     GPL3???                                                                           */
/* Created:     2018-05-08                                                                        */
/* Edited:      2018-05-08                                                                        */
/* Authors: mogulkan                                                                              */
/* Author crypto signature:                                                                       */
/* Title:                                                                                         */
/* Type:                                                                                          */
/* Purpose                                                                                        */
/* Description:                                                                                   */
/* Notice:                                                                                        */
/*////////////////////////////////////////////////////////////////////////////////////////////////*/
