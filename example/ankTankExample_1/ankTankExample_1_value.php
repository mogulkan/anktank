<?php
declare(strict_types = 1);

namespace mogulkan\ankTank;

require 'ankTankExample_1.php';
/**
 * @var callable $rtnHtmlError
 */


#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-   EXAMPLE HOW TO USE ANKTANK -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
$TMP_array    = array();
$TMP_subArray = array();
/* Example #1.   Set value for not sent variable.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_NAME_F, $TMP_subArray, ankTankI::C_STRING, 'Joe');

/* Example #2.   Chop very long string, instead of fail.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_NAME_M, $TMP_subArray, ankTankI::C_STRING, null, true);

/* Example #3.   Set value for invalid value, instead of fail or chop.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_NAME_L, $TMP_subArray, ankTankI::C_STRING, 'Doe');

/* Example #4.   Change execution flow base on variable check.*/
if(ankTankExample_1::validateAndPut(ankTankExample_1::T_BANK_CARD_CVV, $TMP_subArray, ankTankI::C_INT))
{
    // Any action if variable passed check.
}
else
{
  // Action if variable was n't passed check.
/* Example #5.   Change execution flow if required variable was n't sent at all.*/
     ankTankExample_1::setStatusNotSend(                   ankTankExample_1::T_BANK_CARD_CVV                           );
  if(ankTankExample_1::chkStatusByType( ankTankI::C_ARRAY, ankTankExample_1::T_BANK_CARD_CVV, ankTankI::E_VAR_NOT_SEND))
  {
    // Variable is required to script execution, but it was n't sent.
    $varBankCardCVV = 'not found';// Variable for put break point on.
  }
}

/* Example #6.   Accpeting value by string or number.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_BANK_CARD_EXP_M, $TMP_subArray, ankTankI::C_INT);

/* Example #7.   Accepting value witch checking "from's source". Tty send expY by GET metho via typeing expY variable derectly in brouser's address bar.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_BANK_CARD_EXP_Y, $TMP_subArray, ankTankI::C_INT);

///* Example #8.   Get valid variable.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_AGREED, $agreed      , ankTankI::C_BOOL                         );// Set to variable.
ankTankExample_1::validateAndPut(ankTankExample_1::T_AGREED, $TMP_array   , ankTankI::C_BOOL, null, false,         '');// Add to array.
ankTankExample_1::validateAndPut(ankTankExample_1::T_AGREED, $TMP_array   , ankTankI::C_BOOL, null, false, 'subArray');// Add to sub array with custom key.
ankTankExample_1::validateAndPut(ankTankExample_1::T_AGREED, $TMP_subArray, ankTankI::C_BOOL                         );// Add to standard sub array. ankTankA::P_VLD

/* Example #9.   Forbid some value, any other is acceptable.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_CURRENCY, $TMP_subArray, ankTankI::C_STRING, ankTankExample_1::A_VAL_CURRENCY_CMN[ 0 ]);

/* Example #10.   Common valudetermined by RegExp.*/
/* Example #11.   Callback function.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_BANK_CARD_NUMBER, $TMP_subArray, ankTankI::C_INT);

/* Example #12.   .*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_RADIO_SET, $TMP_subArray, ankTankI::C_INT);

/* Example #13.   Set special setting for quantity from array.*/
ankTankExample_1::validateAndPut(ankTankExample_1::T_QUANTITY, $TMP_subArray, ankTankI::C_INT);

/**
 * @var array $vld - ankTankExample_1::P_VLD
 */
extract($TMP_subArray);


$htmlInput_agree = empty($agreed) ? '' : ' checked';

$htmlInput_set = array_fill(0, 2, '');
if(isset($vld[ ankTankExample_1::T_RADIO_SET ])){$htmlInput_set[ ($vld[ ankTankExample_1::T_RADIO_SET ] ? 1 : 0) ] = ' checked' ;}

$htmlInput_cur = array_fill_keys(ankTankExample_1::A_VAL_CURRENCY_CMN, '');
if(isset($vld[ ankTankExample_1::T_CURRENCY  ])){$htmlInput_cur[ $vld[ ankTankExample_1::T_CURRENCY             ]] = ' selected';}





#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\-            RETURN            -/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\#
echo 
  "<!DOCTYPE html>".
  "<html lang='en'>".
    "<head>".
      "<title>AnkTank examle #1 sending by one value.</title>".
      "<style>".
        "html>body>form{".
          "width:640px;".
          "margin:auto;".
          "font-size:2em;".
        "}".
        "html>body>form>div{".
          "display:grid;".
          "grid-template-columns:240px 360px;".
          "grid-row-gap:10px;".
          "padding:20px;".
          "background-color:darksalmon;".
        "}".
        "html>body>form>div>input[type='radio'],".
        "html>body>form>div>input[type='checkbox']{".
          "margin-right:380px;".
          "transform:scale(2);".
        "}".
        "html>body>form>div>div>input{".
          "width:60px;".
        "}".
        "html>body>form>div>div>span{".
          "padding:0 20px;".
        "}".
        "html>body>form>p{padding:40px}".
        "html>body>form>p[data-check='0']{background-color:pink}".
        "html>body>form>p[data-check='1']{background-color:lightgreen}".
        "html>body>form>p[data-check='2']{background-color:yellow}".
        "html>body>form>button{".
          "width:640px;".
          "font-size:2em".
        "}".
      "</style>".
    "</head>".
  
  
  
    "<body>".
      "<form method='post'>".
        "<h1>Send as one value</h1>".
        "<h2>Card</h2>".
        "<div>".
          "<label>First name</label><input".                                                      " value='".( $vld[ ankTankExample_1::T_NAME_F           ] ?? '' )."' type=text>".
          "<label>Long middle name</label><input name=".    ankTankExample_1::T_NAME_M           ." value='".( $vld[ ankTankExample_1::T_NAME_M           ] ?? '' )."' type=text>".
          "<label>Last name</label><input name=".           ankTankExample_1::T_NAME_L           ." value='".( $vld[ ankTankExample_1::T_NAME_L           ] ?? '' )."' type=text>".
          "<label>Bank card number</label><textarea name=". ankTankExample_1::T_BANK_CARD_NUMBER .">".(        $vld[ ankTankExample_1::T_BANK_CARD_NUMBER ] ?? '' )."</textarea>".
          "<label>Bank card CVV</label><input name=".       ankTankExample_1::T_BANK_CARD_CVV    ." value='".( $vld[ ankTankExample_1::T_BANK_CARD_CVV    ] ?? '' )."' type=text>".
          "<label>Expire</label><div><input name=".         ankTankExample_1::T_BANK_CARD_EXP_M  ." value='".( $vld[ ankTankExample_1::T_BANK_CARD_EXP_M  ] ?? '' )."' type=text><".
                     "span> / </span><input name=".         ankTankExample_1::T_BANK_CARD_EXP_Y  ." value='".( $vld[ ankTankExample_1::T_BANK_CARD_EXP_Y  ] ?? '' )."' type=text></div>".
          "<label>Agree</label><input name=".               ankTankExample_1::T_AGREED           ." value='1'".                                 $htmlInput_agree ." type=checkbox>".
          "<label>set</label><input name=".                 ankTankExample_1::T_RADIO_SET        ." value='1'".                              $htmlInput_set[ 1 ] ." type=radio>".
          "<label>not set</label><input name=".             ankTankExample_1::T_RADIO_SET        ." value='0'".                              $htmlInput_set[ 0 ] ." type=radio>".
          "<label>Curency</label><select name=".            ankTankExample_1::T_CURRENCY                                                                               .">".
                                      "<option value='".    ankTankExample_1::A_VAL_CURRENCY_CMN[ 0 ] ."'". $htmlInput_cur[ ankTankExample_1::A_VAL_CURRENCY_CMN[ 0 ]] .">".  ankTankExample_1::A_VAL_CURRENCY_CMN[ 0 ] ." - default</option>".
                                      "<option value='".    ankTankExample_1::A_VAL_CURRENCY_CMN[ 1 ] ."'". $htmlInput_cur[ ankTankExample_1::A_VAL_CURRENCY_CMN[ 1 ]] .">".  ankTankExample_1::A_VAL_CURRENCY_CMN[ 1 ] ."</option>".
                                      "<option value='".    ankTankExample_1::A_VAL_CURRENCY_CMN[ 2 ] ."'". $htmlInput_cur[ ankTankExample_1::A_VAL_CURRENCY_CMN[ 2 ]] .">".  ankTankExample_1::A_VAL_CURRENCY_CMN[ 2 ] ."</option>".
                                      "<option value='".    ankTankExample_1::A_VAL_CURRENCY_FRB[ 0 ]                                                                  ."'>". ankTankExample_1::A_VAL_CURRENCY_FRB[ 0 ] ." - not accept</option>".// This value never will being checked.
                                                                                                            "</select>".
          "<label>Quanitty</label><input name=".            ankTankExample_1::T_QUANTITY         ." value='".( $vld[ ankTankExample_1::T_QUANTITY  ] ?? '' )."' type=text>".
          "<label>Unsecured variable</label><input name=UnsecuredVariable value='' type=text placeholder='I am always fail, because there is not rule for me'>".
        "</div>".
  

        "<button type='submit'>submit</button>".
        $rtnHtmlError(ankTankExample_1::T_NAME_F          ) .
        $rtnHtmlError(ankTankExample_1::T_NAME_M          ) .
        $rtnHtmlError(ankTankExample_1::T_NAME_L          ) .
        $rtnHtmlError(ankTankExample_1::T_BANK_CARD_NUMBER) .
        $rtnHtmlError(ankTankExample_1::T_BANK_CARD_CVV   ) .
        $rtnHtmlError(ankTankExample_1::T_BANK_CARD_EXP_M ) .
        $rtnHtmlError(ankTankExample_1::T_BANK_CARD_EXP_Y ) .
        $rtnHtmlError(ankTankExample_1::T_AGREED          ) .
        $rtnHtmlError(ankTankExample_1::T_RADIO_SET       ) .
        $rtnHtmlError(ankTankExample_1::T_CURRENCY        ) .
        $rtnHtmlError(ankTankExample_1::T_QUANTITY        ) .
        $rtnHtmlError('UnsecuredVariable'                 ) .
        "<button type='submit'>submit</button>".


      "</form>".
    "</body>".
  "</html>"
;

if(empty($_POST)){echo '<br>$_POST is empty';}
else
{
  echo 'What script got: var_dump($_POST)';
  var_dump($_POST);
}
/*  
 * variable ankTankExample_1::T_AGREED is not present because we put it in to variable $agreed. 
 * variable ankTankExample_1::T_NAME_L is not present because don't allow to get variable by HTTP method GET. 
 */
if(empty($vld)){echo '<br>$vld is empty';}
else
{
  echo '<br>What passed ankTank filter rules: var_dump($vld)';
  var_dump($vld);
}


/*////////////////////////////////////////////////////////////////////////////////////////////////*/
/* License:     GPL3???                                                                           */
/* Created:     2018-05-08                                                                        */
/* Edited:      2018-05-08                                                                        */
/* Authors: mogulkan                                                                              */
/* Author crypto signature:                                                                       */
/* Title:                                                                                         */
/* Type:                                                                                          */
/* Purpose                                                                                        */
/* Description:                                                                                   */
/* Notice:                                                                                        */
/*////////////////////////////////////////////////////////////////////////////////////////////////*/
