<?php
declare(strict_types = 1);

namespace mogulkan\ankTank;

interface ankTankI
{
  /** Public constant of known PHP types. */
  public const C_NULL      = 0;//
  public const C_BOOL      = 1;// 
  public const C_INT       = 2;// 
  public const C_FLOAT     = 3;// 
  public const C_STRING    = 4;// 
  public const C_ARRAY     = 5;// 
  public const C_OBJECT    = 6;// 
  public const C_RESOURCE  = 7;//
  public const A_C2T_TYPES = array(
    ankTankI::C_NULL     =>     'null',
    ankTankI::C_BOOL     =>     'bool',
    ankTankI::C_INT      =>      'int',
    ankTankI::C_FLOAT    =>    'float',
    ankTankI::C_STRING   =>   'string',
    ankTankI::C_ARRAY    =>    'array',
    ankTankI::C_OBJECT   =>   'object',
    ankTankI::C_RESOURCE => 'resource',
  );
  
  public const D_RECURSION_DISABLED =                                  0;// Use it as constant for function calling, instead of passing not-self-descriptive number.
  public const F_ZERO               = 0b00000000000000000000000000000000;//              

  public const F_FROM_GET      = 0b0000000001;// Variable can come from PHP $_GET.
  public const F_FROM_POST     = 0b0000000010;// Variable can come from PHP $_POST.
  public const F_FROM_FILES    = 0b0000000100;// Variable can come from PHP $_FILES.
  public const F_FROM_COOKIE   = 0b0000001000;// Variable can come from PHP $_COOKIE.
  public const F_FROM_REFERER  = 0b0000010000;// Variable can come from PHP $_SERVER[ HTTP_REFERER ].
  public const F_FROM_SERVER   = 0b0000100000;// Variable can come from PHP $_SERVER.
  public const F_FROM_VAULT    = 0b0001000000;// Variable can come from vault.
  public const F_FROM_METHOD   = 000010000000;// Variable can come from method.
  public const F_FROM_FUNCTION = 000100000000;// Variable can come from function.
  
  /*
   * Array rule F_CMN_ARRAY_SIMPLE or F_CMN_ARRAY_COMPLEX must be set to check arrays. Both can be set same time, for double check.
   */
  public const F_CMN_DISABLED            = 0b00000000000000000000000000000001;// Rule is set but variable disabled and check always fail with E_VAR_DISABLED.
  public const F_CMN_EMPTY_VAL           = 0b00000000000000000000000010000000;// Variable can have empty string value. Only actual for string, array and object values.
  public const F_CMN_LOG                 = 0b00000000000000000000000100000000;// Result fo checking variable should be stored.
  public const F_CMN_LOG_ERROR           = 0b00000000000000000000001000000000;// Result fo checking variable should be stored if error occurs.
  public const F_CMN_ONLY_VALUE          = 0b00000000000000000000000000000010;// Variable must have only common values.
  public const F_CMN_AT_LEAST_ONE_REGEXP = 0b00000000000000000000000000000100;// Variable must have only common values matched to regexp at least once.
  public const F_CMN_TRY_FIX_LEN         = 0b00000000000000000000010000000000;// If variable have wrong length function static::fixValLength(...) will being called.
  public const F_CMN_ARRAY_SIMPLE        = 0b00000000000000000000100000000000;// Array check rule. Array item variable will checked by   array name. Same name different type.
  public const F_CMN_ARRAY_COMPLEX       = 0b00000000000000000001000000000000;// Array check rule. Array item variable will checked by complex name. ArrayName + underscore + (non-numeric array key)
  public const F_CMN_ARRAY_WHOLE         = 0b00000000000000000010000000000000;// Array check rule. All array's variables check passed or error.
  public const F_CMN_RECURSION_NULL      = 0b00000000000000000100000000000000;// If recursion limit is reached, check is passed, value set to null.
  public const F_CMN_RECURSION_EMPTY     = 0b00000000000000001000000000000000;// If recursion limit is reached, check is passed, value set to empty array / object.
  public const F_CMN_RECURSION_FAIL      = 0b00000000000000010000000000000000;// If recursion limit is reached, check is failed. 
  public const F_CMN_RECURSION_PASS      = 0b00000000000000100000000000000000;// If recursion limit is reached, check is passed, value put as is. DANGEROUS! 

//// Constant flag.
  /* Must be set!
   * Possible binary flag is described in ankTankI::F_CMN_*
   */
  public const C_RULE_BIN_CMN        =  1;// Common binary settings.
  /* Must be set! 
   * Possible binary flag is described in ankTankI::F_FROM_*
   */
  public const C_RULE_BIN_FROM       =  2;// Binary settings for variable income source.
  public const C_RULE_MORE           =  3;// Equal to C_RULE_MORE, but > used as comparison.
  public const C_RULE_LESS           =  4;// Equal to C_RULE_LESS, but < used as comparison.
  public const C_RULE_MIN            =  5;// Minimal length for strings, minimal value for numbers, minimal amount for array. 
  public const C_RULE_MAX            =  6;// Maximal length for strings, maximal value for numbers, maximal amount for array.
  public const C_RULE_EQUAL          =  7;// Exact value length.
  /*
   * Rule     definition: ankTankI::C_RULE_CALLBACK => '\mogulkan\someFunction', ankTankI::C_RULE_RECURSION => 1,
   * Function definition: function someFunction(int $from, $var, &$value, bool $pbChop, ?int $recursionProtector, int $varLen, int $valType): int.
   */
  public const C_RULE_CALLBACK       =  8;// Special function title for calling back.
  /* 
   * Only for array, object and complex data that used recursion to check value. 
   * If not defined ankTankA::C_RECURSION_PROTECTOR is used. 2 is minimum.
   * Common binary settings ankTankI::F_CMN_RECURSION_*
   */
  public const C_RULE_RECURSION      = 9;// How many sub elements a variable can have.  
  /* not applicable for array, object and complex data... yet.
   * ankTankI::C_RULE_CMN, ankTankI::C_RULE_CMN_REGEX_ALL, ankTankI::C_RULE_CMN_REGEX_ANY,
   * ankTankI::C_RULE_FRB, ankTankI::C_RULE_FRB_REGEX_ALL, ankTankI::C_RULE_FRB_REGEX_ANY,
   */  
  public const C_RULE_CMN            = 10;// Sub array with common values.    Strict comparison with type checking.
  public const C_RULE_CMN_REGEX_ALL  = 11;// Sub array with common regular expression. Check is passed if all regExp was passed successfully.
  public const C_RULE_CMN_REGEX_ANY  = 12;// Sub array with common regular expression. Check is passed if any regExp was passed successfully.
  public const C_RULE_FRB            = 13;// Sub array with forbidden values. Strict comparison with type checking. 
  public const C_RULE_FRB_REGEX_ALL  = 14;// Sub array with forbidden regular expression. Check is passed if all  regExp was n't passed successfully.
  public const C_RULE_FRB_REGEX_ANY  = 15;// Sub array with forbidden regular expression. Check is passed if none regExp was n't passed successfully.
  
  
  public const F_VAR_LOG_STATUS_DISABLED = ankTankI::F_ZERO;//
  public const F_VAR_LOG_STATUS_CACHE    = 0b0000000001;//
  public const F_VAR_LOG_STATUS_PASSED   = 0b0000000001;//
  public const F_VAR_LOG_STATUS_FAILED   = 0b0000000000;//
  
  /* Check status codes. */  
  public const R_VAR_CHK_PASS                =  -1;// Variable passed the check.
  public const R_VAR_CHK_PASS_NOT_SEND       =  -2;// Variable haven't been sent, default value was set.
  public const R_VAR_CHK_PASS_FAIL           =  -3;// Variable check fail, default value was set.

  public const E_VAR_UNK                     =  1;// Check failed because variable haven't defined rule to make a check. Can't be logged for security reason.
  public const E_VAR_UNK_RULE                =  2;// Variable haven't rule.                                              Can't be logged for security reason.
  public const E_VAR_UNK_TYPE                =  3;// Variable have unknown type.                                         Can't be logged for security reason.
  public const E_VAR_NOT_SEND                =  4;// Check failed because variable haven't send.
  public const E_VAR_DISABLED                =  5;// Variable disabled by setting rule. ankTankI::F_CMN_ALLOWED
  public const E_VAR_CHK_FAIL                =  6;//
  public const E_VAR_TOO_SMALL               =  7;//
  public const E_VAR_TOO_BIG                 =  8;//

  public const E_VAL_UNK                     = 100;//
  public const E_VAL_TYPE_MISMATCH           = 101;// Variable check return unexpected type of value. Expected type set by $valType.
  public const E_VAL_FROM_FAIL               = 102;//
  public const E_VAL_ARRAY_FAIL_ALL          = 103;//
  public const E_VAL_ARRAY_FAIL_WHOLE        = 104;// Some sub elements can't pass check, but ankTankI::F_CMN_ARRAY_WHOLE required all pass or fail. 
  
  public const E_VAL_EMPTY                   = 110;//
  public const E_VAL_LENGTH_NOT_EQUAL        = 111;// Length not equal to setting rule length.
  public const E_VAL_TOO_SMALL               = 112;//
  public const E_VAL_TOO_BIG                 = 113;//
  
  public const E_VAL_CMN_NO_COMMON           = 120;// Value don't matched to any known common     value. it's fail if ankTankI::F_CMN_ONLY_VALUE          was set.  
  public const E_VAL_CMN_NO_COMMON_REGEXP    = 121;// Value don't matched to any known regexp for value. it's fail if ankTankI::F_CMN_AT_LEAST_ONE_REGEXP was set.
  public const E_VAL_CMN_REGEXP_ALL_FAIL     = 122;// preg_match return     0, that mean value not found by regexp.
  public const E_VAL_CMN_REGEXP_ALL_ERROR    = 123;// preg_match return false, that mean error occurs.
  public const E_VAL_CMN_REGEXP_ANY_FAIL     = 124;//
  public const E_VAL_CMN_REGEXP_ANY_ERROR    = 125;//

  public const E_VAL_FRB                     = 130;// Value is forbidden and also crucial don't have forbidden value.
  public const E_VAL_FRB_REGEXP_ALL          = 131;// 
  public const E_VAL_FRB_REGEXP_ANY_FAIL     = 132;// 
  public const E_VAL_FRB_REGEXP_ANY_ERROR    = 133;//

  public const E_VAL_CALLBACK_FAIL           = 140;//
  public const E_VAL_RECURSION_LIMIT_REACHED = 141;// 
  public const E_VAL_CALLBACK_NOT_FOUND      = 142;//
  
  /* This is explanation for programmer neither visitor.
   * Please implement your solution.  
   */
  public const A_LANG_ERR_MSG = array(
    ankTankI::R_VAR_CHK_PASS                => 'Check is passed.',
    ankTankI::R_VAR_CHK_PASS_NOT_SEND       => 'Check is passed. Variable has n\'t been sent. Default value was used.',
    ankTankI::R_VAR_CHK_PASS_FAIL           => 'Check is passed. Variable has n\'t have valid value. Default value was used.',
  
    ankTankI::E_VAR_UNK                     => 'Unknown error.',
    ankTankI::E_VAR_UNK_RULE                => 'Variable haven\'t rule in ankTankC::A_RULE_CACHE for given type',
    ankTankI::E_VAR_UNK_TYPE                => 'Variable have unknown type or type no allowed for variable',
    ankTankI::E_VAR_NOT_SEND                => 'Variable was n\'t send',
    ankTankI::E_VAR_DISABLED                => 'Variable disabled',
    ankTankI::E_VAR_CHK_FAIL                => 'Check failed',
    ankTankI::E_VAR_TOO_SMALL               => 'Variable name is too small',
    ankTankI::E_VAR_TOO_BIG                 => 'Variable name is to big',
  
    ankTankI::E_VAL_UNK                     => 'Error is unknown',
    ankTankI::E_VAL_TYPE_MISMATCH           => 'Unexpected type. Value have different type before and after validation',
    ankTankI::E_VAL_FROM_FAIL               => 'variable come from not allowed income ',
    ankTankI::E_VAL_ARRAY_FAIL_ALL          => 'None of sub elements passed the check.',
    ankTankI::E_VAL_ARRAY_FAIL_WHOLE        => 'Some of sub elements was n\'t passed the check.',
  
    ankTankI::E_VAL_EMPTY                   => 'value is empty',
    ankTankI::E_VAL_LENGTH_NOT_EQUAL        => 'VValue length is not equal.',
    ankTankI::E_VAL_TOO_SMALL               => 'Value is too small.',
    ankTankI::E_VAL_TOO_BIG                 => 'Value is too big.',
  
    ankTankI::E_VAL_CMN_NO_COMMON           => 'Variable haven\'t common value',
    ankTankI::E_VAL_CMN_NO_COMMON_REGEXP    => 'Variable haven\'t common regexp',
    ankTankI::E_VAL_CMN_REGEXP_ALL_FAIL     => 'Value did n\'t pass all common regexp',
    ankTankI::E_VAL_CMN_REGEXP_ALL_ERROR    => 'Error occurs while checking all common regexp',
    ankTankI::E_VAL_CMN_REGEXP_ANY_FAIL     => 'Value haven\'t matched to any common regexp.',
    ankTankI::E_VAL_CMN_REGEXP_ANY_ERROR    => 'Error occurs while checking any common regexp',
  
    ankTankI::E_VAL_FRB                     => 'Variable have forbidden value',
    ankTankI::E_VAL_FRB_REGEXP_ALL          => 'Value matched to all forbidden regexp',
    ankTankI::E_VAL_FRB_REGEXP_ANY_FAIL     => 'Value matched to one of more forbidden regexp',
    ankTankI::E_VAL_FRB_REGEXP_ANY_ERROR    => 'Error occurs while checking any forbidden regexp',
  
    ankTankI::E_VAL_CALLBACK_FAIL           => 'Callback check failed',
    ankTankI::E_VAL_RECURSION_LIMIT_REACHED => 'Callback check failed',
);
  
}

/*////////////////////////////////////////////////////////////////////////////////////////////////*/
/* License:     GPL3???                                                                           */
/* Created:     2019-04-15                                                                        */
/* Edited:      2019-04-15                                                                        */
/* Authors: mogulkan                                                                              */
/* Authors's crypto signature:                                                                    */
/* Title:                                                                                         */
/* Type:                                                                                          */
/* Purpose                                                                                        */
/* Description:                                                                                   */
/* Notice:                                                                                        */
/*////////////////////////////////////////////////////////////////////////////////////////////////*/
