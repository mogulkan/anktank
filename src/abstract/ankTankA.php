<?php
declare(strict_types=1);

namespace mogulkan\ankTank;
/**
 * Class ankTankA
 * @author  mogulkan
 * @package anktank
 */
class ankTankA implements ankTankI
{
  public const P_VLD                 = 'vld';// Array title that contain validated data. Used in ankTankA::validateAndPut(***)
  public const T_ARRAY_DELIMITER     =   '_';//
  public const C_RECURSION_PROTECTOR =     2;//
  
  public const G_VAR_NAME_MIN_LENGTH = array(
    ankTankI::F_FROM_GET      => 1,
    ankTankI::F_FROM_POST     => 1,
    ankTankI::F_FROM_FILES    => 1,
    ankTankI::F_FROM_COOKIE   => 1,
    ankTankI::F_FROM_SERVER   => 1,
    ankTankI::F_FROM_VAULT    => 1,
    ankTankI::F_FROM_METHOD   => 1,
    ankTankI::F_FROM_FUNCTION => 1,
  );// Minimal length for variable name.
  public const G_VAR_NAME_MAX_LENGTH = array(
    ankTankI::F_FROM_GET      => 25,
    ankTankI::F_FROM_POST     => 25,
    ankTankI::F_FROM_FILES    => 25,
    ankTankI::F_FROM_COOKIE   => 25,
    ankTankI::F_FROM_SERVER   => 25,
    ankTankI::F_FROM_VAULT    => 25,
    ankTankI::F_FROM_METHOD   => 25,
    ankTankI::F_FROM_FUNCTION => 25,
  );// Maximal length for variable name.
  
  
/**
  public const A_RULE_CACHE[ VALUE_TYPE ][ NAME_CHAR_NUMBER ][ NAME ]
  0  => float  longMin         - Минимальная  длинна значения переменной/сколько элементов может содержать массив.
  1  => float  longMax         - Максимальная длинна значения переменной/сколько элементов может содержать массив.
  2  => int    errorLogLevel   -
  3  => string varFunction     - Специальная функция котрая будет прменена к переменной. Имя функции аналогично названию перемнной.

  4  => bool   commonValue     - В значении переменной разрешенны значение только из Common если да то только значение из common, иначе проверка на запрещённые значения и пропустить.
  4  => bool   commonValueOnly - В значении переменной разрешенны значение только из Common если да то только значение из common, иначе проверка на запрещённые значения и пропустить.
  4  => bool   denyValue       - В значении переменной разрешенны значение только из Common если да то только значение из common, иначе проверка на запрещённые значения и пропустить.
  4  => bool   denyValSqlCom   - Запрещает коментарии MySQL.
  4  => bool   denyValSpace    - Запрещает пробел в значении переменной а так же эквиваленты в MySQL %09,%0A,%0D,%0B,%0C.
  4  => bool   denyQuoteSlash  - Запрещает ' " ` / \


//  public const A_RULE_CACHE = array(
//    ankTankI::C_INT => array(
//      1 => array(
//        '' => array(ankTankI::C_RULE_MIN => 0.0, ankTankI::C_RULE_MORE =>  0,
//                    ankTankI::C_RULE_MAX => 0.0, ankTankI::C_RULE_LESS => 13, ankTankI::C_RULE_CALLBACK => '',
//                    ankTankI::C_RULE_BIN_CMN  => ankTankI::F_ZERO,
//                    ankTankI::C_RULE_BIN_FROM => ankTankI::F_FROM_POST,
//                    ankTankI::C_RULE_CMN => array(), ankTankI::C_RULE_CMN_REGEX_ANY => array(), ankTankI::C_RULE_FRB => array(), ankTankI::C_RULE_FRB_REGEX_ANY => array()
//        ),
//      ),
//    ),
//  );
*/
  public const A_RULE_CACHE = array();// Constant cache.

  
// Traits.
  use ankTankT_MethodStaticPublic_chk;
  use ankTankT_MethodStaticPublic_checkVar;
  use ankTankT_MethodStaticPublic_getters;
  use ankTankT_MethodStaticPublic_rtn;
  use ankTankT_MethodStaticProtected_setters;
  use ankTankT_MethodStaticPublic_validate;
  use ankTankT_propertyStaticProtected;
  use ankTankT_MethodStaticPublic_CallBack;
  use ankTankT_MethodStaticPublic_setters;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created:     2017.07.18                                                                        //
// Edited:      2017.07.18                                                                        //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
