<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


trait ankTankT_MethodStaticPublic_setters
{
  public static function setStatusNotSend($varName): bool 
  {
    # Assign variables.
    $varLen  = static::rtnVarLength($varName               );
    $valType = static::rtnValueType($varName, null, $varLen); 
        if(is_null($valType                                             )){return false;}
    elseif(empty(static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ])){return false;}
  
    # Processing.
    $status    = static::getVarStatusByType($valType, $varLen, $varName);
    if(is_null($status))
   {
     $sendFrom = static::chkVarWasSend($varName);
     if(empty($sendFrom))
     {
       static::$varStatus[ $valType ][ $varLen ][ $varName ] = ankTankI::E_VAR_NOT_SEND;
       return true;
     }
   }
  
  # Return.
    return false;
  }
  
  
  
  
  
  
  
  
  
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created: 2017.07.17                                                                            //
// Edited:  2017.07.17                                                                            //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
