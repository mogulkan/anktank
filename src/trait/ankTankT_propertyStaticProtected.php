<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


trait ankTankT_propertyStaticProtected
{
  protected static $varStatus    = array();
  protected static $varValidated = array();
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created:     2017.07.18                                                                        //
// Edited:      2017.07.18                                                                        //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
