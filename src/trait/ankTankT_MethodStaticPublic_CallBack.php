<?php
declare(strict_types = 1);

namespace mogulkan\ankTank;


trait ankTankT_MethodStaticPublic_CallBack
{
  public static function cbDomain($var, &$value, int $varLen, int $valType): ?int
  {
    $parsedUrl = parse_url($value);
    if(empty($parsedUrl)){return ankTankI::E_VAR_CHK_FAIL;}
  # Return.
    return ankTankI::R_VAR_CHK_PASS;
  }


}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created:     2017-08-23                                                                        //
// Edited:      2017-08-23                                                                        //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose                                                                                        //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
