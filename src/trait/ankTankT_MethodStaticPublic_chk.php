<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


/**
 * Trait ankTankT_MethodStaticProtected_chk
 *
 * @package mogulkan
 */
trait ankTankT_MethodStaticPublic_chk
{
  /**
   * @param int            $valType - ankTankI constant. ankTankI::C_*
   * @param int            $varLen  - variable character length.
   * @param int | string   $var     - variable name and array keys.
   * @param mixed          $value   - any value.
   *                        
   * @return int                    - ankTankI::R_VAR_CHK_PASS or ankTankI::E_VAL_
   */
  public static function chkValLength(int $valType, int $varLen, $var, &$value): int
  {
    # Assign variables.
        if(ankTankI::C_STRING == $valType){$countElement =       mb_strlen($value);}
    elseif(ankTankI::C_ARRAY  == $valType){$countElement =           count($value);}
    elseif(ankTankI::C_OBJECT == $valType){$countElement = get_object_vars($value);}
    else                                  {$countElement =                &$value ;}

  # Check.
  ## Check emptiness for if it's allowed.
    if(empty($value) && true == (ankTankI::F_CMN_EMPTY_VAL & static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_BIN_CMN ]))
    {
          if(ankTankI::C_BOOL   == $valType &&      empty($value)){return ankTankI::R_VAR_CHK_PASS;}
      elseif(ankTankI::C_INT    == $valType && 0 == $countElement){return ankTankI::R_VAR_CHK_PASS;}
      elseif(ankTankI::C_FLOAT  == $valType && 0 == $countElement){return ankTankI::R_VAR_CHK_PASS;}
      elseif(ankTankI::C_STRING == $valType && 0 == $countElement){return ankTankI::R_VAR_CHK_PASS;}
      elseif(ankTankI::C_ARRAY  == $valType && 0 == $countElement){return ankTankI::R_VAR_CHK_PASS;}
      elseif(ankTankI::C_OBJECT == $valType && 0 == $countElement){return ankTankI::R_VAR_CHK_PASS;}
      else                                                        {return ankTankI::E_VAL_EMPTY   ;}
    }// If value isn't empty. it should pass next min/max value check.
  ## Check empty value range, if it disallowed.
    elseif($countElement === ''                                                                                                                                                                                                                                                                  ){return ankTankI::E_VAL_EMPTY           ;}// Empty value for non string variables.
    elseif($countElement === 0                                                                                                                                                                                                                                                                   ){return ankTankI::E_VAL_EMPTY           ;}
  ## Check min/max value range.
    elseif(isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_EQUAL ]) &&                                                                                                 $countElement != static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_EQUAL ]){return ankTankI::E_VAL_LENGTH_NOT_EQUAL;}
    elseif(isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MORE  ]) && $countElement < static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MORE ]                                                                                                  ){return ankTankI::E_VAL_TOO_SMALL       ;}
    elseif(isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_LESS  ]) && $countElement > static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_LESS ]                                                                                                  ){return ankTankI::E_VAL_TOO_BIG         ;}
    elseif(isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MIN   ]) && $countElement < static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MIN  ] && $countElement != static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MIN   ]){return ankTankI::E_VAL_TOO_SMALL       ;}
    elseif(isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MAX   ]) && $countElement > static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MAX  ] && $countElement != static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MAX   ]){return ankTankI::E_VAL_TOO_BIG         ;}

  # Return.
    return ankTankI::R_VAR_CHK_PASS;
  }
  
  
  
  
  
  
  
  
  
  
  /**
   * @param int            $valType - ankTankI constant. ankTankI::C_*
   * @param int            $varLen  - variable character length.
   * @param int | string   $var     - variable name and array keys.
   * @param mixed          $value   - any value.
   *
   * @return bool
   */
  public static function fixValLength(int $valType, int $varLen, $var, &$value): bool
  {
  # Check.
        if(empty($value)                                                                                 ){return false;}
    elseif($valType != ankTankI::C_INT && $valType != ankTankI::C_FLOAT && $valType != ankTankI::C_STRING){return false;}
    elseif(((isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MIN  ]) || isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MORE ])) == false)
         &&((isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MAX  ]) || isset(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_LESS ])) == false)
    ){return false;}

  # Assign variables.
    if(ankTankI::C_FLOAT == $valType)
    {
      $lenMin = static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MIN ] ?? static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MORE ] ?? PHP_FLOAT_MIN;
      $lenMax = static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MAX ] ?? static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_LESS ] ?? PHP_FLOAT_MAX;

      if($lenMin < PHP_FLOAT_MIN){$lenMin = PHP_FLOAT_MIN;}
      if($lenMax > PHP_FLOAT_MAX){$lenMin = PHP_FLOAT_MAX;}
    }
    else
    {
      $lenMin = static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MIN ] ?? static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MORE ] ?? PHP_INT_MIN;
      $lenMax = static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_MAX ] ?? static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_LESS ] ?? PHP_INT_MAX;
  
      if($lenMin < PHP_INT_MIN){$lenMin =       66535;}// Bigger padding may cause freeze or shutdown, if padding happening.
      if($lenMax > PHP_INT_MAX){$lenMax = PHP_INT_MAX;}
    }
  
  # Processing.
    if(ankTankI::C_STRING == $valType)
    {
      if($lenMin < 0){$lenMin = 0;}
  
             $valLen = mb_strlen($value);
          if($valLen < $lenMin){$value =   str_pad($value,    $lenMin,' ');}
      elseif($valLen > $lenMax){$value = mb_substr($value, 0, $lenMax    );}
    }
    else
    {
          if($value < $lenMin){$value = $lenMin;}
      elseif($value > $lenMax){$value = $lenMax;}
    }

  # Return.
    return true;
  }
  
  
  
  
  
  
  
  
  
  
  /** Checking common values.
   *
   * @param int            $valType - ankTankI constant. ankTankI::C_*
   * @param int            $varLen  - variable character length.
   * @param int | string   $var     - variable name and array keys.
   * @param mixed          $value   - any value.
   *
   * @return int                    - ankTankI::R_VAR_CHK_PASS or ankTankI::E_VAL_
   */
  public static function chkVarByCommonValues(int $valType, int $varLen, $var, &$value): int
  {
        if(empty(                                           static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN     ]               )){return ankTankI::E_VAL_CMN_NO_COMMON;}// Rule was passed because no rules specified. Rule and check skip.
    elseif(                      in_array(         $value , static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN     ], true) === true){return ankTankI::R_VAR_CHK_PASS     ;}// Value matched to common value.
    elseif(is_numeric($value) && in_array(intval(  $value), static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN     ], true) === true){return ankTankI::R_VAR_CHK_PASS     ;}// Value matched to common value. Convert because all value send as string.
    elseif(is_numeric($value) && in_array(floatval($value), static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN     ], true) === true){return ankTankI::R_VAR_CHK_PASS     ;}// Value matched to common value. Convert because all value send as string.
    elseif(                    ankTankI::F_CMN_ONLY_VALUE & static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_BIN_CMN ]                ){return ankTankI::E_VAR_CHK_FAIL     ;}// Value failed to have only common value.
    else                                                                                                                                                    {return ankTankI::E_VAL_CMN_NO_COMMON;}// Value haven't common value and it's acceptable.
  }
  
  
  
  
  
  
  
  
  
  
  /** Checking forbidden value.
   *
   * @param int            $valType - ankTankI constant. ankTankI::C_*
   * @param int            $varLen  - variable character length.
   * @param int | string   $var     - variable name and array keys.
   * @param mixed          $value   - any value.
   *
   * @return int                    - ankTankI::R_VAR_CHK_PASS or ankTankI::E_VAL_
   */
  public static function chkVarByForbiddenValues(int $valType, int $varLen, $var, &$value): int
  {
  # Check
        if(empty(           static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_FRB ])                ){return ankTankI::R_VAR_CHK_PASS;}// Rule was passed because no rules specified. Rule and check skip.
    elseif(in_array($value, static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_FRB ], true) === false){return ankTankI::R_VAR_CHK_PASS;}

  # Return.
    return ankTankI::E_VAL_FRB;
  }
  
  
  
  
  
  
  
  
  
  
  public static function chkVarByForbiddenRegExpAll(int $valType, int $varLen, $var, &$value): int
  {
    # Check
    if(empty(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_FRB_REGEX_ALL ])){return ankTankI::R_VAR_CHK_PASS;}

    # Processing.
    foreach( static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_FRB_REGEX_ALL ] as $valRegExp)
    {
      $result = preg_match("/$valRegExp/", $value);
      if($result){return ankTankI::R_VAR_CHK_PASS;}
    }

    # Return.
    return ankTankI::E_VAL_FRB_REGEXP_ALL;
  }
  
  
  
  
  
  
  
  
  
  
  public static function chkVarByForbiddenRegExpAny(int $valType, int $varLen, $var, &$value): int
  {
  # Check
    if(empty(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_FRB_REGEX_ANY ])){return ankTankI::R_VAR_CHK_PASS;}// Rule was passed because no rules specified. Rule and check skip.
  
  # Processing.
    foreach( static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_FRB_REGEX_ANY ] as $valRegExp)
    {
      $result = preg_match("/$valRegExp/", $value);
          if(          $result){return ankTankI::E_VAL_FRB_REGEXP_ANY_FAIL ;}
      elseif(false === $result){return ankTankI::E_VAL_FRB_REGEXP_ANY_ERROR;}
    }
  
  # Return.
      return ankTankI::R_VAR_CHK_PASS;
  }
  
  
  
  
  
  
  
  
  
  
  public static function chkVarByCommonRegExpAny(int $valType, int $varLen, $var, &$value): int
  {
  # Check
    if(empty(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN_REGEX_ANY ])){return ankTankI::R_VAR_CHK_PASS;}
  
  # Processing.
    $TMP_array = array();
    foreach( static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN_REGEX_ANY ] as $valRegExp)
    {
      $result = preg_match("/$valRegExp/", $value, $TMP_array);
      if($result){return ankTankI::R_VAR_CHK_PASS;}
    }

  # Return.
    return (ankTankI::F_CMN_AT_LEAST_ONE_REGEXP & static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_BIN_CMN ]) ? ankTankI::E_VAL_CMN_REGEXP_ANY_FAIL : ankTankI::E_VAL_CMN_NO_COMMON_REGEXP;
  }
  
  
  
  
  
  
  
  
  
  
  public static function chkVarByCommonRegExpAll(int $valType, int $varLen, $var, &$value): int
  {
  # Check
    if(empty(static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN_REGEX_ALL ])){return ankTankI::R_VAR_CHK_PASS;}
  
  # Processing.
    foreach( static::A_RULE_CACHE[ $valType ][ $varLen ][ $var ][ ankTankI::C_RULE_CMN_REGEX_ALL ] as $valRegExp)
    {
      $result = preg_match("/$valRegExp/", $value);
          if(    0 === $result){return ankTankI::E_VAL_CMN_REGEXP_ALL_FAIL ;}
      elseif(false === $result){return ankTankI::E_VAL_CMN_REGEXP_ALL_ERROR;}
    }

  # Return.
    return ankTankI::R_VAR_CHK_PASS;
  }
  
  
  
  
  
  
  
  
  
  
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created:     2017.07.18                                                                        //
// Edited:      2017.07.18                                                                        //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
