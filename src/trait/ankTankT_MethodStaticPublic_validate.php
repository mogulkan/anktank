<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


trait ankTankT_MethodStaticPublic_validate
{
  /**
   * @param string          $varName
   * @param                 $RTN_var
   * @param int|null        $valType    - Value type one of ankTankI::A_C2T_TYPES. 
   * @param null            $valDfl
   * @param bool            $pbChop     - value will be truncated, if maximal length is more than allowed.
   * @param string          $psArrayKey = validusC::F_VALIDUS_VALIDAtED
   * @param bool            $dflNull
   * @param int             $statusLog  - How variable checking status c=should be logged.
   *
   * @return bool
   */
  public static function validateAndPut(string $varName, &$RTN_var = null, ?int $valType = null, $valDfl = null, bool $pbChop = false, string $psArrayKey = ankTankA::P_VLD, bool $dflNull = false, int $statusLog = ankTankI::F_VAR_LOG_STATUS_CACHE): bool
  {
  # Assign variables.
    $varLen  = static::rtnVarLength($varName);
    if(is_null($valType)){$valType = static::rtnValueTypeRule($varName, $varLen);}

        if(is_null($valType                                             )){return false;}
    elseif(empty(static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ])){return false;}


  # Check variable was sent.
    $validValue = static::getData($varName);
    if(is_null($validValue))
    {
          if(isset($_POST[   $varName ])){$varStatus = static::validate($varName, $_POST[   $varName ], static::F_FROM_POST  , $valType, true, false, $pbChop, $statusLog);}
      elseif(isset($_GET[    $varName ])){$varStatus = static::validate($varName, $_GET[    $varName ], static::F_FROM_GET   , $valType, true, false, $pbChop, $statusLog);}
      elseif(isset($_COOKIE[ $varName ])){$varStatus = static::validate($varName, $_COOKIE[ $varName ], static::F_FROM_COOKIE, $valType, true, false, $pbChop, $statusLog);}
      elseif(isset($_SERVER[ $varName ])){$varStatus = static::validate($varName, $_SERVER[ $varName ], static::F_FROM_SERVER, $valType, true, false, $pbChop, $statusLog);}
      elseif(isset($_FILES[  $varName ])){$varStatus = static::validate($varName, $_FILES[  $varName ], static::F_FROM_FILES , $valType, true, false, $pbChop, $statusLog);}
      else                               {$varStatus =                                                                                            ankTankI::E_VAR_NOT_SEND;}
    }else                                {$varStatus =                                                             static::getVarStatusByType($valType, $varLen, $varName);}


  # If variable have passed the cj=heck, set default value.
    if(ankTankI::R_VAR_CHK_PASS != $varStatus && ankTankI::R_VAR_CHK_PASS_NOT_SEND != $varStatus && ankTankI::R_VAR_CHK_PASS_FAIL != $varStatus)
    {
      if(isset($valDfl))
      {
        $result       = static::setDataDflByType($valType, $varLen, $varName, $valDfl, true);
        if($result)
        {
          $statusNew  = ankTankI::E_VAR_NOT_SEND == $varStatus ? ankTankI::R_VAR_CHK_PASS_NOT_SEND : ankTankI::R_VAR_CHK_PASS_FAIL;
          $validValue = static::getDataByType(   $valType, $varLen, $varName                              );
          $result     = static::setStatusByType( $valType, $varLen, $varName, $statusNew, true, $statusLog);
        }
      }
    }
    elseif(is_null($validValue))
    {
      $validValue = static::getData($varName);
    }
  

  # Return.
    if(is_null($RTN_var))// Validate and put, but not set to return reference.
    {
      if($dflNull && isset($valDfl)){$RTN_var = $valDfl;}// Case when $RTN_var is defined in method invocation. 
      
          if(isset($validValue)   && (static::rtnValueType($varName, $validValue, $varLen) == $valType)){                                                   return  true;}
      elseif(true == $dflNull                                                                          ){                                                   return false;}
      elseif(is_null($valDfl)                                                                          ){                                                   return false;}
      else                                                                                              {                                                   return  true;}
    }
    elseif(is_array($RTN_var))
    {
      if(empty($psArrayKey))// Don't put validated variable in sub array.
      {
        if(ankTankI::C_ARRAY == $valType && is_array($validValue)){return false;}
  
            if(isset($validValue) && (static::rtnValueType($varName, $validValue, $varLen) == $valType)){$RTN_var[ $varName ] =                $validValue; return  true;}
        elseif(true == $dflNull                                                                        ){$RTN_var[ $varName ] =                       null; return false;}
        elseif(is_null($valDfl)                                                                        ){                                                   return false;}
        else                                                                                            {$RTN_var[ $varName ] =                    $valDfl; return  true;}
      }
      else// Put validated variable in sub array.
      {
            if(isset($validValue) && (static::rtnValueType($varName, $validValue, $varLen) == $valType)){$RTN_var[ $psArrayKey ][ $varName ] = $validValue; return  true;}
        elseif(true == $dflNull                                                                        ){$RTN_var[ $psArrayKey ][ $varName ] =        null; return false;}
        elseif(is_null($valDfl)                                                                        ){                                                   return false;}
        else                                                                                            {$RTN_var[ $psArrayKey ][ $varName ] =     $valDfl; return  true;}
      }
    }
    else
    {
          if(isset($validValue)   && (static::rtnValueType($varName, $validValue, $varLen) == $valType)){$RTN_var =                            $validValue; return  true;}
      elseif(true == $dflNull                                                                          ){$RTN_var =                                   null; return false;}
      elseif(is_null($valDfl)                                                                          ){                                                   return false;}
      else                                                                                              {$RTN_var =                                $valDfl; return  true;}
    }
  }
  
  
  
  
  
  
  
  
  
  
  /** Validate value.
   * 1.Validate value
   * 2.Set to static cache(by default)
   * 3.Fix length (optional)
   * 4 Return status.
   *
   * @param                 $varName        - variable name like ankTankA::T_*.
   * @param                 $val            - variable value.
   * @param int             $income         - variable income like ankTankI::F_FROM_*.
   * @param int|null        $valType        - Value type one of ankTankI::A_C2T_TYPES.
   * @param bool            $isSet          - save variable to cache if variable successfully checked.
   * @param bool            $isForceReCheck - force variable recheck, if it's already passed check and set to cache.
   * @param bool            $pbChop         - value will be truncated, if maximal length is more than allowed.
   * @param int             $statusLog      - force to set status of validation. Values:.
   *
   * @return int                 - return constant ankTankI::R_VAR_* or ankTankI::R_VAR_CHK_PASS.
   */
  public static function validate($varName, &$val, int $income, ?int $valType = null, bool $isSet = true, bool $isForceReCheck = false, bool $pbChop = false, int $statusLog = ankTankI::F_VAR_LOG_STATUS_CACHE): int
  {
  # Assign variables.
    $varLen  = static::rtnVarLength($varName);
    if(is_null($valType)){$valType = static::rtnValueTypeRule($varName, $varLen);}


  # Check.
        if(is_null($valType                                             )){return static::rtnValueTypeRule($varName, $varLen) == null ? ankTankI::E_VAR_UNK_RULE : ankTankI::E_VAR_UNK_TYPE;}
    elseif(empty(static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ])){return                                                       ankTankI::E_VAR_UNK_RULE                           ;}
  # Skip check if already validated.
    elseif(false == $isForceReCheck)
    {
          if(isset(static::$varValidated[ $valType ][ $varLen ][ $varName ])){return ankTankI::R_VAR_CHK_PASS;}
//      elseif(isset(static::$dataParsed[    $valType ][ $varLen ][ $varName ])){return ankTankI::R_VAR_CHK_PASS;}#FF
    }


  # Processing.
    $statusNew = static::checkVar($income, $varName, $val, $pbChop);


  # Save variable to static cache.
        if(false == $isSet                                                                            ){}
    elseif(false == $isForceReCheck && isset(static::$varValidated[ $valType ][ $varLen ][ $varName ])){}
    elseif(ankTankI::R_VAR_CHK_PASS == $statusNew)
    {
      if(static::rtnValueType($varName, $val, $varLen) == $valType){static::setDataByType($valType, $varLen, $varName, $val, $isForceReCheck);}
      else                                                         {$statusNew =                                ankTankI::E_VAL_TYPE_MISMATCH;}
    }
    

  # Set log status.
    static::setStatusByType($valType, $varLen, $varName, $statusNew, $isForceReCheck, $statusLog);
    
    
  # Return.
    return $statusNew;
  }
  
  
  
  
  
  
  
  
  
  
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created: 2017.07.17                                                                            //
// Edited:  2017.07.17                                                                            //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
