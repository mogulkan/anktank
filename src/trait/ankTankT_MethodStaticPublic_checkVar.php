<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


trait ankTankT_MethodStaticPublic_checkVar
{
  /**
   * Check variable.
   *
   * 1  Check variable name.
   * 1.1 Check variable name length.
   * 2.2.1 Check variable value type.
   * 1.3.2 Check variable name is expected, common and known by system.
   * 2.3 Check variable income.
   *
   * 2   Check variable value.
   * 2.1 Check variable by callback function if one was defined before.
   * 2.1.1 Check variable value by emptiness. Only actual for string, array and object values.
   * 2.1.2 Check variable value for min/max value range.
   * 2.1 Check variable default common and forbidden values.
   * 2.1 Check variable for special type examination.
   * 2.1 Convert variable to discovered type.
   *
   * @param int   $from               - откуда пришла данная переменная(GET/POST/FILES/COOKIE/FB(from base))/SERVER(fromWeb server)/FF(from function)
   * @param mixed $varName            - переменная которую необходимо проверить,
   * @param mixed $value              -
   * @param bool  $pbChop             - value will be truncated, if maximal length is more than allowed.
   * @param int   $recursionProtector - used only for array checks.
   *
   *
   * @return int
   */
  public static function checkVar(int $from, $varName, &$value, bool $pbChop = false, ?int $recursionProtector = null)
  {
  # Check variable name.
  ## Check variable length.
    $varLen = static::rtnVarLength($varName);
        if(static::G_VAR_NAME_MIN_LENGTH[ $from ] > $varLen){return ankTankI::E_VAR_TOO_SMALL;}
    elseif(static::G_VAR_NAME_MAX_LENGTH[ $from ] < $varLen){return ankTankI::E_VAR_TOO_BIG ;}
    
    
  # Check common name.
  ## If we haven't rule for the variable, that mean the variable is unknown. Test fail.
    if(empty(static::A_RULE_CACHE[ ankTankI::C_BOOL   ][ $varLen ][ $varName ])
    && empty(static::A_RULE_CACHE[ ankTankI::C_INT    ][ $varLen ][ $varName ])
    && empty(static::A_RULE_CACHE[ ankTankI::C_FLOAT  ][ $varLen ][ $varName ])
    && empty(static::A_RULE_CACHE[ ankTankI::C_STRING ][ $varLen ][ $varName ])
    && empty(static::A_RULE_CACHE[ ankTankI::C_ARRAY  ][ $varLen ][ $varName ])
    ){return ankTankI::E_VAR_UNK;}

    $valType = static::rtnValueType($varName, $value, $varLen);
        if(is_null($valType)                                                                                             ){return static::rtnValueType($varName, null, $varLen) == null ? ankTankI::E_VAR_UNK_RULE : ankTankI::E_VAR_UNK_TYPE;}
    elseif(ankTankI::F_CMN_DISABLED & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){return                                                                                    ankTankI::E_VAR_DISABLED;}



  # Check var income  - откуда поступила данная переменная.
    if(($from & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_FROM ]) == false){return ankTankI::E_VAL_FROM_FAIL;}



  # Check value.
  ## Calling call method. If there is none callback method is defined null will be returned and check will continue.
        if(empty(       static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ static::C_RULE_CALLBACK ])){}
    elseif((is_callable(static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ static::C_RULE_CALLBACK ], false, $specialMethod)
       || method_exists(static::class, $specialMethod)) 
      && (is_null($recursionProtector) || $recursionProtector > 0)
    ){
      # Assign variables. Recursion protector.
      if(is_null($recursionProtector))
      {
        $recursionProtector = static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_RECURSION ] ?? static::C_RECURSION_PROTECTOR;
      }
      elseif(--$recursionProtector < 1)
      {
            if(ankTankI::F_CMN_RECURSION_FAIL  & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){$value = array(); return ankTankI::E_VAL_RECURSION_LIMIT_REACHED;}
        elseif(ankTankI::F_CMN_RECURSION_NULL  & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){$value =    null; return ankTankI::R_VAR_CHK_PASS               ;}
        elseif(ankTankI::F_CMN_RECURSION_EMPTY & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){$value = array(); return ankTankI::R_VAR_CHK_PASS               ;}
        elseif(ankTankI::F_CMN_RECURSION_PASS  & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){                  return ankTankI::R_VAR_CHK_PASS               ;}
        else                                                                                                                         {$value =    null; return ankTankI::E_VAL_RECURSION_LIMIT_REACHED;}
      }
  
  
          if(function_exists(               $specialMethod)){return         $specialMethod($from, $varName, $value, $pbChop, $recursionProtector, $varLen, $valType);}
      elseif(method_exists(  static::class, $specialMethod)){return static::$specialMethod($from, $varName, $value, $pbChop, $recursionProtector, $varLen, $valType);}
      else                                                  {return                                                               ankTankI::E_VAL_CALLBACK_NOT_FOUND;}
    }



  ## Check value length.
    $result = static::chkValLength($valType, $varLen, $varName, $value);
        if(ankTankI::R_VAR_CHK_PASS == $result                                                                                                             ){                             }
    elseif(ankTankI::E_VAL_EMPTY    == $result                                                                                                             ){return ankTankI::E_VAL_EMPTY;}
    elseif(false == $pbChop && (ankTankI::F_CMN_TRY_FIX_LEN & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]) == false){return               $result;}
    elseif(static::fixValLength($valType, $varLen, $varName, $value)                                                                                       ){                             }// If true length is fixed.
    else                                                                                                                                                    {return               $result;}// Else return chkValLength() error.



  ## Check default common and forbidden values for variable.
    if(ankTankI::C_INT == $valType || ankTankI::C_FLOAT == $valType || ankTankI::C_STRING == $valType || ankTankI::C_BOOL == $valType)
    {
      $result = static::chkVarByCommonValues($valType, $varLen, $varName, $value);
          if(ankTankI::E_VAR_CHK_FAIL      == $result){return ankTankI::E_VAR_CHK_FAIL;}
      elseif(ankTankI::E_VAL_CMN_NO_COMMON == $result)// Only if no common values was found make search for forbidden values.
      {
        $result = static::chkVarByForbiddenValues($valType, $varLen, $varName, $value);
            if(ankTankI::E_VAL_FRB == $result ){return ankTankI::E_VAR_CHK_FAIL;}
        elseif(ankTankI::C_STRING  == $valType)
        {
        # Check by regExp.
          $result = static::chkVarByForbiddenRegExpAll($valType, $varLen, $varName, $value);
          if(ankTankI::R_VAR_CHK_PASS != $result){return $result;}

          $result = static::chkVarByForbiddenRegExpAny($valType, $varLen, $varName, $value);
          if(ankTankI::R_VAR_CHK_PASS != $result){return $result;}
      
          $result = static::chkVarByCommonRegExpAny($valType, $varLen, $varName, $value);
              if(ankTankI::R_VAR_CHK_PASS             == $result){} 
          elseif(ankTankI::E_VAL_CMN_NO_COMMON_REGEXP == $result)
          {
            $result = static::chkVarByCommonRegExpAll($valType, $varLen, $varName, $value);
            if($result != ankTankI::R_VAR_CHK_PASS){return $result;}
          }
          else{return $result;}
        }
      }
    }



  ## Convert to type.
        if(ankTankI::C_BOOL   == $valType){settype($value,   'bool');}
    elseif(ankTankI::C_INT    == $valType){settype($value,    'int');}
    elseif(ankTankI::C_FLOAT  == $valType){settype($value,  'float');}
    elseif(ankTankI::C_STRING == $valType){settype($value, 'string');}
  ## Check array values.
    elseif(ankTankI::C_ARRAY == $valType)
    {
    # Assign variables. Recursion protector.
      if(is_null($recursionProtector))
      {
        $recursionProtector = static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_RECURSION ] ?? static::C_RECURSION_PROTECTOR;
      }
      elseif(--$recursionProtector < 1)
      {
            if(ankTankI::F_CMN_RECURSION_FAIL  & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){$value = array(); return ankTankI::E_VAL_RECURSION_LIMIT_REACHED;}
        elseif(ankTankI::F_CMN_RECURSION_NULL  & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){$value =    null; return ankTankI::R_VAR_CHK_PASS               ;}
        elseif(ankTankI::F_CMN_RECURSION_EMPTY & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){$value = array(); return ankTankI::R_VAR_CHK_PASS               ;}
        elseif(ankTankI::F_CMN_RECURSION_PASS  & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){                  return ankTankI::R_VAR_CHK_PASS               ;}
      }
  
      $cleanValue = array();
      foreach($value as $keyX=>$valX)
      {
      # If array complex name is allowed.
        if(ankTankI::F_CMN_ARRAY_COMPLEX & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ])
        {
          if(is_numeric($keyX)){$result = static::checkVar($from, $varName.static::T_ARRAY_DELIMITER      , $valX, $pbChop, $recursionProtector);}
          else                 {$result = static::checkVar($from, $varName.static::T_ARRAY_DELIMITER.$keyX, $valX, $pbChop, $recursionProtector);}
      
              if(ankTankI::R_VAR_CHK_PASS === $result                                                                             ){$cleanValue[ $keyX ] = $valX;  continue;}
          elseif(ankTankI::F_CMN_ARRAY_WHOLE & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){return ankTankI::E_VAL_ARRAY_FAIL_WHOLE;}
        }
  
      # If array simple name is allowed.
        if(ankTankI::F_CMN_ARRAY_SIMPLE & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ])
        {
          $result = static::checkVar($from, $varName, $valX, $pbChop, $recursionProtector);
              if(ankTankI::R_VAR_CHK_PASS === $result                                                                             ){$cleanValue[ $keyX ] = $valX;  continue;}
          elseif(ankTankI::F_CMN_ARRAY_WHOLE & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){return ankTankI::E_VAL_ARRAY_FAIL_WHOLE;}
        }
      }
  
          if(!empty($value) && empty($cleanValue) && false == (ankTankI::F_CMN_EMPTY_VAL   & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ])){return ankTankI::E_VAL_ARRAY_FAIL_ALL  ;}
      elseif( count($value) != count($cleanValue) &&  true == (ankTankI::F_CMN_ARRAY_WHOLE & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ])){return ankTankI::E_VAL_ARRAY_FAIL_WHOLE;}
  
      $value = $cleanValue;
    }



  # Return.
    return ankTankI::R_VAR_CHK_PASS;
  }








  
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created:     2017.07.18                                                                        //
// Edited:      2017.07.18                                                                        //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
