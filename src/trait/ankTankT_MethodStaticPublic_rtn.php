<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


trait ankTankT_MethodStaticPublic_rtn
{
  /** Return variable type as int constant ankTankI::C_.
   * @param                 $varName        -
   * @param mixed           $value          - If value is null, first existed check rule will be returned. 
   * @param int             $varLen         - Variable length. ankTankA::rtnVarLength(*)
   *
   * @return int | null
   * @example $valType = ankTankA::rtnValueType(ankTankA::T_VAR_NAME, $value, rtnVarLength(ankTankA::T_VAR_NAME));
   */
  public static function rtnValueType($varName, $value, int $varLen): ?int
  {
        if(is_null($value )){return null;}
    elseif(is_array($value))
    {
      if(     isset(static::A_RULE_CACHE[ ankTankI::C_ARRAY ][ $varLen ][ $varName ])){return ankTankI::C_ARRAY ;}
    }
    elseif(   isset(static::A_RULE_CACHE[ ankTankI::C_BOOL  ][ $varLen ][ $varName ])){return ankTankI::C_BOOL  ;}
    elseif(is_numeric($value) || empty($value))
    {
          if(isset(static::A_RULE_CACHE[ ankTankI::C_FLOAT  ][ $varLen ][ $varName ])){return ankTankI::C_FLOAT ;}
      elseif(isset(static::A_RULE_CACHE[ ankTankI::C_INT    ][ $varLen ][ $varName ])){return ankTankI::C_INT   ;}
      elseif(isset(static::A_RULE_CACHE[ ankTankI::C_STRING ][ $varLen ][ $varName ])){return ankTankI::C_STRING;}// Super gigantic number, treated as string.
    }
    elseif(is_string($value) 
          && isset(static::A_RULE_CACHE[ ankTankI::C_STRING ][ $varLen ][ $varName ])){return ankTankI::C_STRING;}

    # Return.
    return null;
  }
  
  
  
  
  
  
  
  
  
  
  public static function rtnValueTypeRule($varName, int $varLen): ?int
  {
        if(isset(static::A_RULE_CACHE[ ankTankI::C_ARRAY  ][ $varLen ][ $varName ])){return ankTankI::C_ARRAY ;}
    elseif(isset(static::A_RULE_CACHE[ ankTankI::C_BOOL   ][ $varLen ][ $varName ])){return ankTankI::C_BOOL  ;}
    elseif(isset(static::A_RULE_CACHE[ ankTankI::C_FLOAT  ][ $varLen ][ $varName ])){return ankTankI::C_FLOAT ;}
    elseif(isset(static::A_RULE_CACHE[ ankTankI::C_INT    ][ $varLen ][ $varName ])){return ankTankI::C_INT   ;}
    elseif(isset(static::A_RULE_CACHE[ ankTankI::C_STRING ][ $varLen ][ $varName ])){return ankTankI::C_STRING;}
    else                                                                            {return               null;}
  }  
  
  
  
  
  
  
  
  
  
  /** Return variable name length.
   * Override this if you use not PHP variable compatible name for variables.
   * 
   * @param $varName
   *
   * @return int
   */
  public static function rtnVarLength($varName): int     {return strlen($varName);}
  
  
  
  
  
  
  
  
  
  
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created:     2017.07.18                                                                        //
// Edited:      2017.07.18                                                                        //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
