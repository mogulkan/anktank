<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


trait ankTankT_MethodStaticPublic_getters
{
  public static function getDataByType(int $valType, int $varLen, $varName)
  {
    return isset(static::$varValidated[ $valType ][ $varLen ][ $varName ]) ? static::$varValidated[ $valType ][ $varLen ][ $varName ] : null;
  }
  
  
  
  
  
  
  
  
  
  
  public static function getData($varName, ?int $valType = null)
  {
    # Assign variables.
    $varLen = static::rtnVarLength($varName);
  
    if(is_null($valType))
    {
          if(isset(static::$varValidated[ ankTankI::C_BOOL     ][ $varLen ][ $varName ])){return static::$varValidated[ ankTankI::C_BOOL     ][ $varLen ][ $varName ];}
      elseif(isset(static::$varValidated[ ankTankI::C_INT      ][ $varLen ][ $varName ])){return static::$varValidated[ ankTankI::C_INT      ][ $varLen ][ $varName ];}
      elseif(isset(static::$varValidated[ ankTankI::C_FLOAT    ][ $varLen ][ $varName ])){return static::$varValidated[ ankTankI::C_FLOAT    ][ $varLen ][ $varName ];}
      elseif(isset(static::$varValidated[ ankTankI::C_STRING   ][ $varLen ][ $varName ])){return static::$varValidated[ ankTankI::C_STRING   ][ $varLen ][ $varName ];}
      elseif(isset(static::$varValidated[ ankTankI::C_ARRAY    ][ $varLen ][ $varName ])){return static::$varValidated[ ankTankI::C_ARRAY    ][ $varLen ][ $varName ];}
      elseif(isset(static::$varValidated[ ankTankI::C_OBJECT   ][ $varLen ][ $varName ])){return static::$varValidated[ ankTankI::C_OBJECT   ][ $varLen ][ $varName ];}
      elseif(isset(static::$varValidated[ ankTankI::C_RESOURCE ][ $varLen ][ $varName ])){return static::$varValidated[ ankTankI::C_RESOURCE ][ $varLen ][ $varName ];}
    } elseif(isset(static::$varValidated[             $valType ][ $varLen ][ $varName ])){return static::$varValidated[             $valType ][ $varLen ][ $varName ];}
  
  # Return.
    return null;
  }
  
  
  
  
  
  
  
  
  
  
  public static function getVarStatus($varName)
  {
    $varLen = static::rtnVarLength($varName);

        if(isset(static::$varStatus[     ankTankI::C_BOOL     ][ $varLen ][ $varName ])){return static::$varStatus[ ankTankI::C_BOOL     ][ $varLen ][ $varName ];}
    elseif(isset(static::$varStatus[     ankTankI::C_INT      ][ $varLen ][ $varName ])){return static::$varStatus[ ankTankI::C_INT      ][ $varLen ][ $varName ];}
    elseif(isset(static::$varStatus[     ankTankI::C_FLOAT    ][ $varLen ][ $varName ])){return static::$varStatus[ ankTankI::C_FLOAT    ][ $varLen ][ $varName ];}
    elseif(isset(static::$varStatus[     ankTankI::C_STRING   ][ $varLen ][ $varName ])){return static::$varStatus[ ankTankI::C_STRING   ][ $varLen ][ $varName ];}
    elseif(isset(static::$varStatus[     ankTankI::C_ARRAY    ][ $varLen ][ $varName ])){return static::$varStatus[ ankTankI::C_ARRAY    ][ $varLen ][ $varName ];}
    elseif(isset(static::$varStatus[     ankTankI::C_OBJECT   ][ $varLen ][ $varName ])){return static::$varStatus[ ankTankI::C_OBJECT   ][ $varLen ][ $varName ];}
    elseif(isset(static::$varStatus[     ankTankI::C_RESOURCE ][ $varLen ][ $varName ])){return static::$varStatus[ ankTankI::C_RESOURCE ][ $varLen ][ $varName ];}
    elseif(isset(static::$varValidated[  ankTankI::C_BOOL     ][ $varLen ][ $varName ])){return                                          ankTankI::R_VAR_CHK_PASS;}
    elseif(isset(static::$varValidated[  ankTankI::C_INT      ][ $varLen ][ $varName ])){return                                          ankTankI::R_VAR_CHK_PASS;}
    elseif(isset(static::$varValidated[  ankTankI::C_FLOAT    ][ $varLen ][ $varName ])){return                                          ankTankI::R_VAR_CHK_PASS;}
    elseif(isset(static::$varValidated[  ankTankI::C_STRING   ][ $varLen ][ $varName ])){return                                          ankTankI::R_VAR_CHK_PASS;}
    elseif(isset(static::$varValidated[  ankTankI::C_ARRAY    ][ $varLen ][ $varName ])){return                                          ankTankI::R_VAR_CHK_PASS;}
    elseif(isset(static::$varValidated[  ankTankI::C_OBJECT   ][ $varLen ][ $varName ])){return                                          ankTankI::R_VAR_CHK_PASS;}
    elseif(isset(static::$varValidated[  ankTankI::C_RESOURCE ][ $varLen ][ $varName ])){return                                          ankTankI::R_VAR_CHK_PASS;}
    elseif(empty(static::A_RULE_CACHE[   ankTankI::C_BOOL     ][ $varLen ][ $varName ]) && empty(static::A_RULE_CACHE[ ankTankI::C_INT    ][ $varLen ][ $varName ])
        && empty(static::A_RULE_CACHE[   ankTankI::C_FLOAT    ][ $varLen ][ $varName ]) && empty(static::A_RULE_CACHE[ ankTankI::C_STRING ][ $varLen ][ $varName ])
        && empty(static::A_RULE_CACHE[   ankTankI::C_ARRAY    ][ $varLen ][ $varName ]) && empty(static::A_RULE_CACHE[ ankTankI::C_OBJECT ][ $varLen ][ $varName ])
        && empty(static::A_RULE_CACHE[   ankTankI::C_RESOURCE ][ $varLen ][ $varName ])){return                                          ankTankI::E_VAR_UNK_RULE;}
  
  # Return.
    return null;
  }
  
  
  
  
  
  
  
  
  
  
  public static function  getVarStatusByType(int $valType, int $varLen, $varName)
  {
    return isset(static::$varStatus[ $valType ][ $varLen ][ $varName ]) ? static::$varStatus[ $valType ][ $varLen ][ $varName ] : null;
  }
  
  
  
  
  
  
  
  
  
  
  public static function chkStatusByType(int $valType, $varName, int $status = ankTankI::R_VAR_CHK_PASS): bool
  {
    if(isset(static::$varStatus[ $valType ][ static::rtnVarLength($varName) ][ $varName ]) && static::$varStatus[ $valType ][ static::rtnVarLength($varName) ][ $varName ] == $status){return true;}
  
  # Return.
    return false;  
  }

  
  
  
  
  
  
  
  
  
  
  
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created: 2017.07.17                                                                            //
// Edited:  2017.07.17                                                                            //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
