<?php
declare(strict_types=1);

namespace mogulkan\ankTank;


trait ankTankT_MethodStaticProtected_setters
{
  protected static function setDataByType(int $valType, int $varLen, $varName, $val, bool $override = false)
  {
  # Check.
        if(true == $override                                              ){             }
    elseif(isset(static::$varValidated[ $valType ][ $varLen ][ $varName ])){return false;}
  
  # Processing.
    static::$varValidated[ $valType ][ $varLen ][ $varName ] = $val;
  
  # Return.
    return true;
  }
  
  
  
  
  
  
  
  
  
  
  protected static function setData($varName, $val, bool $override = false)
  {
  # Assign variables.
    $varLen  = static::rtnVarLength($varName);
    $valType = static::rtnValueType($varName, $val, $varLen);
  
  # Check.
        if(true == $override                                              ){             }
    elseif(isset(static::$varValidated[ $valType ][ $varLen ][ $varName ])){return false;}
  
  # Processing.
    static::$varValidated[ $valType ][ $varLen ][ $varName ] = $val;

  # Return.
    return true;
  }
  
  
  
  
  
  
  
  
  
  
  protected static function setDataDflByType(int $valType, int $varLen, $varName, $valDfl, bool $override = false): bool
  {
    # Check.
        if(isset(static::$varValidated[ $valType ][ $varLen ][ $varName ]) && false == $override){return false;}
    elseif(is_null($valDfl)                                                                     ){return false;}
    elseif(ankTankI::C_ARRAY == $valType)
    {
          if(empty(static::$varValidated[ ankTankI::C_ARRAY ][ $varLen ][ $varName ]) && empty($valDfl)){return             false;}
      elseif(!is_array($valDfl)                                                                        ){$valDfl = array($valDfl);}
      
          if(empty(   static::$varValidated[ ankTankI::C_ARRAY ][ $varLen ][ $varName ])){$valOld =                                                                  array();}
      elseif(is_array(static::$varValidated[ ankTankI::C_ARRAY ][ $varLen ][ $varName ])){$valOld =       static::$varValidated[ ankTankI::C_ARRAY ][ $varLen ][ $varName ] ;}
      else                                                                               {$valOld = array(static::$varValidated[ ankTankI::C_ARRAY ][ $varLen ][ $varName ]);}
      
    # Processing.
      $newValue = array_replace_recursive($valDfl, $valOld);
      if(is_null($newValue)){return false;}
    }
    else
    {
      $newValue = $valDfl;
    }

  # Set property.
    static::$varValidated[ $valType ][ $varLen ][ $varName ] = $newValue;

  # Return.
    return true;
  }
  
  
  
  
  
  
  
  
  
  
  protected static function setStatusByType(int $valType, int $varLen, $varName, int $status, bool $override = false, $statusLog = ankTankI::F_VAR_LOG_STATUS_CACHE): bool
  {
  # Check.
        if(isset(static::$varStatus[ $valType ][ $varLen ][ $varName ]) && false == $override){return false;}
    elseif(ankTankI::F_VAR_LOG_STATUS_DISABLED & $statusLog                                  ){return false;}
  # Processing.
    elseif(ankTankI::R_VAR_CHK_PASS          == $status 
        || ankTankI::R_VAR_CHK_PASS_FAIL     == $status
        || ankTankI::R_VAR_CHK_PASS_NOT_SEND == $status 
    ){
          if(ankTankI::F_CMN_LOG               & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){static::$varStatus[ $valType ][ $varLen ][ $varName ] = $status;}
      elseif(ankTankI::F_VAR_LOG_STATUS_PASSED & $statusLog                                                                         ){static::$varStatus[ $valType ][ $varLen ][ $varName ] = $status;}
    }
    else
    {
          if(ankTankI::F_CMN_LOG_ERROR         & static::A_RULE_CACHE[ $valType ][ $varLen ][ $varName ][ ankTankI::C_RULE_BIN_CMN ]){static::$varStatus[ $valType ][ $varLen ][ $varName ] = $status;}
      elseif(ankTankI::F_VAR_LOG_STATUS_FAILED & $statusLog                                                                         ){static::$varStatus[ $valType ][ $varLen ][ $varName ] = $status;}
    }
  
  # Return.
    return true;
  }
  
  
  
  
  
  
  
  
  
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// Created: 2017.07.17                                                                            //
// Edited:  2017.07.17                                                                            //
// Authors: mogulkan                                                                              //
// Title:                                                                                         //
// Type:                                                                                          //
// Purpose:                                                                                       //
// Description:                                                                                   //
// Notice:                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////
